# syntax=docker/dockerfile:1
FROM fedora:latest
WORKDIR /opt

# Install libraries that the engine needs for run and build
RUN dnf -y install libpcap-devel pcre-devel boost boost-devel boost-python3-devel \
     python-devel git python3-devel libtool gcc-c++ make redhat-rpm-config

# Download the master from bitbucket
# If you want a specificy version change the line
RUN cd /opt && git clone https://bitbucket.org/camp0/aiengine src

# Compile the python library
RUN cd /opt/src && bash autogen.sh &&  ./configure && cd src && python3 setup.py build_ext -i && \
    cp pyaiengine*.so /opt/src

# Generate a basic script for run the engine
RUN cd /opt/src && echo "import pyaiengine" >> monitor.py && \
    echo "import sys" >> monitor.py && \
    echo "stack = pyaiengine.StackLan()" >> monitor.py && \
    echo "stack.tcp_flows = 2048 * 128" >> monitor.py && \
    echo "stack.udp_flows = 1024 * 128" >> monitor.py && \
    echo "with pyaiengine.PacketDispatcher('eth0') as pdis:" >> monitor.py && \
    echo "    pdis.stack = stack" >> monitor.py && \
    echo "    pdis.authorized_ip_address = ['127.0.0.1', '172.17.0.0/24']" >> monitor.py && \
    echo "    pdis.http_address = '0.0.0.0'" >> monitor.py && \
    echo "    pdis.http_port = 8080" >> monitor.py && \
    echo "    pdis.enable_shell = True" >> monitor.py && \
    echo "    pdis.run()" >> monitor.py 

# Expose the management port on the engine
EXPOSE 8080

# docker run
ENTRYPOINT ["python3", "/opt/src/monitor.py"]
