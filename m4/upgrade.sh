#!/bin/bash

echo "Upgrading macro files"

for f in `ls ax*.m4`; do
	echo $f
	wget "http://git.savannah.gnu.org/gitweb/?p=autoconf-archive.git;a=blob_plain;f=m4/$f" -O $f
done
