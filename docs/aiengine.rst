=========================
AIEngine description
=========================


.. contents:: Table of Contents


Introduction
------------
The aim of this document is to explain and describe the functionality that AI Engine a New Generation Network Intrusion Detection System engine brings.

AIEngine is a next generation programmable network intrusion detection system. Supports x86_64, ARM and MIPS architecture over operating systems such as Linux, FreeBSD and MacOS.

.. raw:: pdf

   PageBreak

Architecture
------------

The core of AIEngine is a complex library implemented on C++11/14 standard that process packets on real time. This library uses a external layer of high level programming languages, such as Python, Ruby or even Java, that brings to the engine the flexibility of this type of languages and the speed and performance of C++14 standard.

.. image:: aiengine_internal_architecture.png 
   :height: 551 px
   :width: 860 px
   :scale: 150 %
   :alt: AIEngine internal architecture 

All the internal architecture is based on objects that could link or not, depending on customer requirements, with other objects for bring a specific functionality. On the other hand, all the memory connections have internal caches that allows to the system to process more than 5.000.000 concurrent TCP connections with no memory problems.

The system supports the most import protocols for different use cases.

* Banking environments. Support for Bitcoin that allows to the customers monitors, controls and detect potential anomalies on their mining infrastructures.
* IoT infrastructures. Support for the most used protocols for the Internet of Things, and also due to the architecture of the system, could be embedded on small devices.
* Data center environments. Support for the most used protocols for data centers for detect anomalies and potential attacks.
* IMS environments. Nowadays, VoIP servers are target of different type of attacks. The proposed systems brings security to SIP servers in order to deal with the new threats of today.
* Industrial infrastructures. Now is critical to have security systems on Industrial infrastructures that could potentially be attacked. The system implements the most common protocols for this type of environments, bringing more intelligence to the upper layers.

The engine is design to support different network environments such as:

* StackLan: Designed for enterprises based on LAN architectures with MPLS or VLans.
* StackMobile: Designed for Mobile operators that needs security on their GN interfaces for secure their base customers.
* StackLanIPv6: Designed for support IPv6 on LAN architectures.
* StackVirtual: Designed for big data centers that support VxLAn on their architecture.
* StackOpenflow: Designed for data centers that supports OpenFlow (experimental).
* StackMobileIPv6: Designed for Mobile IPv6 operators that needs security on their GN interfaces.

AIEngine supports the programming of customer requirements code on real time. This brings to the engine the capability of deal with new threats with a reacting time close to zero. This code is written in a function that have one parameter, the TCP/UDP connection object, and we called "callbacks". These callbacks can be plugged on different objects.

.. code:: ruby

    # Ruby callback

    def callback_domain(flow)
      print "Malware domain on:%s" % flow 
    end

    d = DomainName.new("Malware domain" ,".some.dns.from.malware.com")
    d.callback = method(:callback_domain)


.. code:: python

    """ Python callback on HTTP traffic """
    
    def callback_zeus(flow):
        h = flow.http_info
        if (h):
            host = str(h.host_name)
            if (host):
                print("Suspicious activity detected on flow", str(flow), host)
                flow.label = "ZeuS malware detected"

    d1 = DomainName("Domain from ZeuS botnet", ".malware.zeus.com")
    d1.callback = callback_zeus
    
    d2 = DomainName("Domain from ZeuS botnet", ".malwarecdn.zeus.com", callback_zeus)

.. code:: java

    // Java callback example

    class ExternalCallback extends JaiCallback{
        public void call(Flow flow) {
            HTTPInfo s = flow.getHTTPInfoObject();
            // Process the HTTPInfo object 
        }
    }

    DomainName d = new DomainName("Generic domain",".generic.com");
    DomainNameManager dm = new DomainNameManager();
    ExternalCallback call = new ExternalCallback();

    d.setCallback(call);
    dm.addDomainName(d);

.. code:: lua

   -- Example of Lua callback

   function domain_callback(flow)
       print("Malware domain on:%s", tostring(flow))
   end

   d = luaiengine.DomainName("Malware domain", ".adjfeixnexeinxt.com")

   dm = luaiengine.DomainNameManager()
   d:set_callback("domain_callback")
   dm:add_domain_name(d)


.. raw:: pdf

   PageBreak

Features
--------

AIEngine supports the following features on version 1.9

.. raw:: pdf

   PageBreak

Supported protocols
~~~~~~~~~~~~~~~~~~~
The engine support the following protocols:

* Bitcoin
    Bitcoin is a new way of generate and interchange money (more info). The system is able to manage the most common options of the protocol, such us, transactions, getdata, getblocks operations and so on.

* CoAP
    The Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained networks in the Internet of Things (IoT). It is particularly targeted for small low power sensors, switches, valves and similar components that need to be controlled or supervised remotely, through standard Internet networks.

* DCERPC
    The Distributed Computing Environment / Remote Procedure Calls (DCERPC) is a protocol designed for write distributed software.

* DHCPv4/DHCPv6
    The Dynamic Host Configuration Protocol (DHCP) provides a quick, automatic, and central management for the distribution of IP addresses within a network.

* DNS
    The Domain Name Service (DNS) is one of the most used protocols on the Internet. DNS provides a way to know the IP address of any host on the Internet. It is no different than any other directory service. From cover channels to Trojans and other type of malware uses DNS for communicate their services. 

* DTLS
    Datagram Transport Layer Security (DTLS) is a communications protocol that provides security for datagram-based applications, the protocol is based on the stream-oriented Transport Layer Security (TLS).

* ETHERNET
    This is the most important protocol for carry LAN datagrams....(TODO).

* GPRS
    The system supports G3 and G4 GPRS versions. This is the most common protocol for Mobile operators on the GN interface.

* GRE
    Nowadays tunnels are very important on Cloud environments. Most of this systems uses isolation of the network in order to prevent security problems with different virtual systems. GRE is one of the most important tunnels system that allows network isolation. Our system supports this protocol in order to bring security to cloud environments.

* HTTP 1.1
    Today HTTP is the most used protocol on the Internet. Also, the majority of the exploit attacks, Trojans, and other type of malware uses this protocol in order to commit different type of ciber-crimes. The proposed system implements a specific HTTP protocol that supports the HTTP 1.1 standard in order to  support multiple request on the same network conversation. 

* ICMPv4/ICMPv6
    The Internet Control Message Protocol (ICMPv4 and ICMPv6) is one of the main protocols of the internet protocol suite. It is used by network devices, like routers, to send error messages indicating, for example, that a requested service is not available or that a host or router could not be reached. Denial of service attacks have been doing by using this protocol, so is key to the system to monitor and react under this type of attacks.

* IMAP
    The Internet Message Access Protocol (IMAP) is an Internet standard protocol used by e-mail clients to retrieve e-mail messages from a mail server over a TCP/IP connection. Attacks that uses invalid credentials or other type of attacks needs to be addresses.

* IPv4/IPv6
    The Internet Protocol (IPv4 and IPv6) is the main communications protocol in the Internet protocol suite for relaying datagrams across network boundaries. This protocol have been involved in many type of attacks, such as fragmentation attacks and so on.

* MPLS
    Multi-Protocol Label Switching (MPLS) provides a mechanism for forwarding packets for any network protocol. MPLS flows are connection-oriented and packets are routed along pre-configured Label Switched Paths (LSPs). All the Network stacks of the system supports MPLS in any of their types.

* Modbus
    Modbus TCP is a communications protocol for use with its programmable logic controllers (PLCs). Simple and robust, it has since become a de facto standard communication protocol, and it is now a commonly available means of connecting industrial electronic devices. This protocol is very important for Industrial systems that needs to monitor and secure their platforms what uses this type of devices.

* MQTT
    MQTT is a publish/subscribe messaging protocol designed for lightweight M2M communications. It was originally developed by IBM and is now an open standard.

* Netbios
    Netbios is a protocol designed for comunication of computers over a LAN.

* NTP
    The Network Time Protocol (NTP) is widely used to synchronize computer clocks in the Internet. The protocol is usually described in terms of a client-server model, but can as easily be used in peer-to-peer relationships where both peers consider the other to be a potential time source. One of the biggest DDoS attacks was made by using this protocol.

* OpenFlow
    OpenFlow is an open standard network protocol used to manage traffic between commercial Ethernet switches, routers and wireless access points. Nowadays, data-centers uses this standard to reduce costs and to manage their networks.

* POP
    The Post Office Protocol (POP) is an application-layer Internet standard protocol used by local e-mail clients to retrieve e-mail from a remote server over a TCP/IP connection. With this protocol users could manage their e-mail for download, delete, store and so on.

* Quic
    The Quic protocol (Quick UDP Internet Connections) is a experimental protocol designed by Google that its goal is to improve perceived performance of connection-oriented web applications that are currently using TCP.

* RTP
    The Real-time Transport Protocol (RTP) defines a standard packet format for delivering audio and video over the Internet. It is defined in RFC 1889. RTP is used extensively in communication and entertainment systems that involve streaming media, such as telephony, video applications, television services and web-based push-to-talk features.

* SIP
    The Session Initiation Protocol (SIP) is an application-layer control (signaling) protocol for creating, modifying, and terminating sessions with one or more participants. These sessions include Internet telephone calls, multimedia distribution, and multimedia conferences. This protocol is used for establish VoIP sessions.

* SMB
    The Server Message Block (SMB) is as an application-layer network protocol used for providing shared access to files in general.

* SMTP
    The Simple Mail Transfer Protocol (SMTP) is a communication protocol for mail servers to transmit email over the Internet. SMTP provides a set of codes that simplify the communication of email messages between email servers. On the other hand, spammers use this protocol to send malware and spam over the Internet.

* SNMP
    The Simple Network Management Protocol (SNMP) is a popular protocol for network management. It is used for collecting information from, and configuring, network devices, such as servers, printers, hubs, switches, and routers on a IP network. SNMP exposes management data in the form of variables on the managed systems, which describe the system configuration. These variables can then be queried (and sometimes set) by managing applications. SNMP have been involved on DDoS reflection attacks on the past, so the system could detect this type of attack and notifies to other systems.

* SSDP
    The Simple Service Discovery Protocol (SSDP) is a network protocol based on the IP suite for advertisement and discovery of network services and presence information. The SSDP protocol can discover Plug & Play devices, with uPnP (Universal Plug and Play). SSDP uses unicast and multicast address (239.255.255.250). SSDP is HTTP like protocol and work with NOTIFY and M-SEARCH methods. This protocol is used for the IoT for discover devices basically.

* SSH
    The Secure Shell (SSH) is a network protocol for operating network services securely over an unsecured networks by using cryptographic functions.

* SSL
    SSL stands for Secure Sockets Layer and was originally created by Netscape. SSLv2 and SSLv3 are the 2 versions of this protocol (SSLv1 was never publicly release). After SSLv3, SSL was renamed to TLS. TLS stands for Transport Layer Security and started with TLSv1.0 which is an upgraded version of SSLv3. The primary goal of the TLS protocol is to provide privacy and data integrity between two communicating computer applications. 

* TCP
    The Transmission Control Protocol (TCP) is a transport layer protocol used by applications that require guaranteed delivery. It is a sliding window protocol that provides handling for both timeouts and retransmissions. On the other hand, TCP establishes a full duplex virtual connection between two endpoints, wherever, each endpoint is defined by an IP address and a TCP port number. The operation of TCP is implemented as a finite state machine. A big varialty of DDoS attacks have been done in the past and recently, incorrect flags, incorrect lengths, offsets and so on.

* UDP
    The User Datagram Protocol (UDP) is an alternative communications protocol to TCP used primarily for establishing low-latency and loss tolerating connections between applications on the Internet.

* VLAN
    A virtual LAN (VLAN) is any broadcast domain that is partitioned and isolated in a computer network at the data link layer. VLANs are use to provide the network segmentation services traditionally provided only by routers in LAN configurations.

* VXLAN
    Virtual Extensible LAN (VXLAN) is a proposed encapsulation protocol for running an overlay network on existing Layer 3 infrastructure. The primary goal of VXLAN is to extend the virtual LAN (VLAN) address space by adding a 24-bit segment ID and increasing the number of available IDs to 16 million.

.. raw:: pdf

   PageBreak

IPSet matching
~~~~~~~~~~~~~~
Most of the engines allows to add sets of IP address in order to monitor or track specific hosts. The engine allows this functionality in a easy way by using the classes IPSet and IPRadixTree. The following example shows how load the IP address from the ToR network and load onto the engine.

.. code:: python

    ipset = IPSet()

    ipset_mng = IPSetManager()
    ipset_mng.add_ip_set(ipset)

    """ Take a big list of IP address that belongs to ToR """
    req = urllib2.Request("https://www.dan.me.uk/torlist/")
    try:
        response = urllib2.urlopen(req)
        for line in response.readlines():
            ip = line.strip()
            try:
    	        socket.inet_aton(ip)
            except:
    	        continue
            ipset.add_ip_address(ip)
    except urllib2.URLError as e:
        print("Error:", e)

    # Sets the IPSetManager on the stack for TCP traffic
    stack.tcp_ip_set_manager = ipset_mng


The comparison about the performance betwwen the IPSet and a IPRadixTree is the following

test 1 is a IPSet with 50.000 ip addresses

.. code:: python
    
    IPSet (IPs)
	Total IP address:            50188
	Total lookups in:                0
	Total lookups out:          192752

test 2 is a IPRadixSet with 50.000 ip addreses

.. code:: python
    
    IPRadixTree (Tree IPs)
	Total IP address:            50188
	Total IP networks:               0
	Total lookups in:                0
	Total lookups out:          192752

test 3 is a IPRadixSet with 9100 B networks covering the 50.000 ip addresses

.. code:: python
    
    IPRadixTree (Tree IPs)
	Total IP address:                0
	Total IP networks:            9109
	Total lookups in:            67137
	Total lookups out:          125615

test 4 is a IPRadixSet with 29800 C networks covering the 50.000 ip addresses

.. code:: python

    IPRadixTree (Tree IPs)
	Total IP address:                0
	Total IP networks:           29879
	Total lookups in:              108
	Total lookups out:          192644

test 5 is a IPBloomSet with 50.000 ip addresses

.. code:: python

    IPBloomSet IPs
	False positive rate:             1
	Total IP address:            50188
	Total lookups in:             2566
	Total lookups out:          190186


+----------+-----------+----------+---------+
| Test     | incl      | heap     |memory   |
+==========+===========+==========+=========+
| Test 1   | 4997404   | 4 MB     | 32,6 MB |
+----------+-----------+----------+---------+
| Test 2   | 693964459 | 8 MB     | 49,9 MB |
+----------+-----------+----------+---------+
| Test 3   | 214737201 | 4,3 MB   | 34,8 MB |
+----------+-----------+----------+---------+
| Test 4   | 245537425 | 5,8 MB   | 42,6 MB |
+----------+-----------+----------+---------+
| Test 5   | 395515316 | 3,6 MB   | 31,7 MB |
+----------+-----------+----------+---------+

The total number of lookups was 192752.

.. raw:: pdf

   PageBreak

Regex graphs
~~~~~~~~~~~~
Nowadays attacks get complex and complex and with Regex Graphs the user is able to generate any complex detection by using graphs. No matter how complex is the attack on the network flow.  Complex detection patterns can be done with this functionality.

.. code:: python

    # Create a basic regex for match generic SSL traffic
    ssl_sig = Regex("SSL Basic regex", b"^\x16\x03")

    # Create another regex for match the heartbeat packets of SSL
    sig = Regex("SSL Heartbeat", b"^.*\x18\x03(\x01|\x02|\x03).*$")

    # Link both regex expressions
    ssl_sig.next_regex = sig

    # Add the main regex to the variable sm of type RegexManager
    sm.add_regex(ssl_sig)

    # Link the sm to the current network stack
    stack.tcp_regex_manager = sm

.. raw:: pdf

   PageBreak

Domain matching
~~~~~~~~~~~~~~~
The system support domain names matching for the protocols HTTP, DNS, SMTP, SSL, QUIC and others. Over HTTP the field Host will be evaluated with a DomainManager that will evaluate if some of the domains matches.

Domains that have a dot as first character include all the subdomains from that entry point. So if we want to include subdomains that we dont know them this is a good option, on the other hand, if match only the exact
domain name dont use the dot on the first character

.. code:: ruby

  d = DomainManager.new
  # The next domain will match all the subdomains from videos.mysite.com
  dom = DomainName.new("Domain from my site", ".videos.mysite.com")
  d.add_domain_name(dom)

  s.set_domain_name_manager(d, "HTTPProtocol")

Also by using DomainNames is possible to generate a sub set of Regex objects. With this functionality the Regex will be more accurate and generate less false positives. For enable this is just as simple as assign a value to a variable.

.. code:: python

  rm = RegexManager()
  dom = DomainName("My specific domain", "customer1.isp.com")
  dom.regex_manager = rm

This functionality is perfect for analyze content on HTTP traffic for unknown malware.

On the DNSProtocol the matching of a specific DNS generates on the data output a JSON packet with all the IPS of the DNS response. This brings to the system the capability to provide DNS records with the IP address response in order to generate threat intelligence.

.. code:: json

  {
    "bytes": 508,
    "info": {
        "dnsdomain": "bubuserve.com",
        "ips": [
            "164.9.107.24",
            "164.9.107.29",
            "164.9.107.12",
            "164.9.107.23",
            "164.9.107.13",
            "164.9.107.16",
            "164.9.107.30",
            "164.9.107.21"
        ],
        "matchs": "Generic domain",
        "qtype": 0
    },
    "ip": {
        "dst": "198.164.30.2",
        "src": "192.168.5.122"
    },
    "layer7": "dns",
    "port": {
        "dst": 53,
        "src": 10886
    },
    "proto": 17
  }

For more details, see `Zeus malware`_ .

.. raw:: pdf

   PageBreak

Ban domain
~~~~~~~~~~
Nowadays the quantity of traffic on the networks is massive, according to bla bla (some references). With this functionality we can exclude traffic that just consume resources on the engine. Facebook, twitter and this services could be used on this. This functionality is used on protocols like HTTP, DNS, SMTP and SSL.

.. code:: python

  dman = DomainManager()
  for dom in list_banned_domains:
      dman.add_domain_name(DomainName("Banned domain", dom))
  
  stack.set_domain_name_manager(dman, "http")
  
.. raw:: pdf

   PageBreak

Memory management
~~~~~~~~~~~~~~~~~

The engine provides two modes of memory management:

- Allocate the memory on boot time (All the memory is allocated when the program starts).

- Allocate the memory dynamically (The memory is allocated depending on the network traffic).

Both modes provides advantages and disadvantages, so depending on your requirements you can choose the model that you want. For example, if you want to run the engine for analyses DNS for malware or monitor Bitcoin transactions, probably your model will be static because you want to allocate all the memory for specific type of traffic. On the other hand, if your system should work as Network Intrusion probably a dynamic mode will be better for you.

All the allocated memory could be clean an refresh in order to have fresh information.

The system provides functionality to increase or decrease specific items of a given protocol, this is useful with static allocation. This allows to make specific configurations for a given protocol. For example a dedicated DNS monitor system what could handle 1.000.000 queries.

.. code:: python

   stack = StackLan()

   stack.tcp_flows = 0
   stack.udp_flows = 1000000

   # Decrease the memory of the rest of UDP protocols
   stack.decrease_allocated_memory(500000, "sip")
   stack.decrease_allocated_memory(500000, "ssdp")

   # Increase the DNSInfos of the DNS protocol
   stack.increase_allocated_memory(1000000, "DNSProtocol")

.. raw:: pdf

   PageBreak

DDoS support
~~~~~~~~~~~~
The engine have mechanisms for support denial of service attacks in the majority of the protocols supported. However, for some complex DDoS attacks the engine is capable to accept specific customer requirements for specific attacks. For using this functionality we use the method add_timer of the PacketDispatcher. This method with combination of the methods get_counters and get_cache from any of the stacks, allows the user to create complex DDoS attack scenarios for a data centers. On the other hand, by using the add_timer method we can schedule task at different times for doing different things, for example find all the connections to a given host that excedes a given quota, get the metrics of a protocol and use a third party framework for math analisys and anomaly detection, and so on.

Here is a basic example for detect TCP syn attacks with ruby.

.. code:: ruby

  def scheduler_handler_tcp

    print "TCP DoS Checker\n"
    c = @s.get_counters("TCPProtocol")
  
    # Code the intelligence for detect DDoS based on 
    # combination flags, bytes, packets and so on. 
    syns = c["syns"]
    synacks = c["synacks"]
    if (syns > (synacks * 100))
      print "System under a SYN DoS attack\n"
    end
  end

Another example for detect attacks over NTP on python

.. code:: python

  def scheduler_handler_ntp():

      total_ips = dict()
      print("NTP DDoS Checker")

      # Count the number different ips of the NTP flows
      for flow in stack.udp_flow_manager:
          if (flow.l7protocol == "NTPProtocol"):
              total_ips[flow.src_ip] = 1

      if (total_ips.len() == len(fu)):
          print("System under a NTP DDoS attack")


   def scheduler_handler_tcp_syn():

       print("Checking TCP connections")
       total_with_no_ack = 0

       for flow in stack.tcp_flow_manager:
           if (flow.tcp_info.syns > 0 and flow.tcp_info.acks == 0):
               total_with_no_acks = total_with_no_acks + 1

       if (totak_with_no_ack > limit):
           print("System under TCP syn attack")

  # On the PacketDispatcher set a timer every 10 seconds
  pdis.add_timer(scheduler_handler_ntp, 10)

All the protocols supports the usage of the stack method get_counters, that allows to extract crucial information from any of the protocols.

You can use this mechanism for detect anomalies that depends on the time and send alerts to other systems.

.. code:: python

  def fragmentation_handler():

      ipstats = stack.get_counters("IP")

      current_ip_packets = ipstats["packets"]
      current_fragmented = ipstats["fragmented packets"]

      if (current_fragmented  > previous_fragments + delta):
          sent_alert("ALERT: IP Fragment attack on the network")

      previous_ip_packets = current_ip_packets
      previous_fragments = current_fragmented

  # On the PacketDispatcher set a timer every 20 seconds
  pdis.add_timer(fragmentation_handler, 20)


.. code:: python

   """ Get statistics of the BitcoinProtocol """
   counters = st.get_counters("bitcoin")
   print(counters)
   {'transaction': 1450, 'get blocks': 200, 'network addr': 4, 'packets': 14963,
    'inv': 1, 'reject': 0, 'bytes': 1476209, 'ping': 0, 'not found': 0, 
    'alert': 0, 'headers': 0, 'getaddr': 24, 'version': 0, 'version ack': 34,
    'get headers': 12, 'pong': 0, 'getdata': 126, 'mempool': 0, 'block': 0}
    

Also timers can be removed with the method remove_timer from the PacketDispatcher

.. raw:: pdf

   PageBreak

Bloom filter support
~~~~~~~~~~~~~~~~~~~~
When the customer requirements needs to track a big number of IP addresses, the IPSets are not enough. For this case, the system implements a bloom filter functionality in order to support this requirement. Notice that bloom filters are fault tolerant caches, so false positives and false negatives could happen. However, depending on the number of IP Address we could recommend their usage.

This option needs to be set on compilation time (--enable-bloomfilter) and also have the boost bloomfilter libraries on the system.

.. raw:: pdf

   PageBreak

Reject TCP/UDP connections
~~~~~~~~~~~~~~~~~~~~~~~~~~
Under some attacks the engine is capable of closing UDP and TCP connections in order to reduce the pressure on the servers and also to disturb the origin of the attack. This functionality is only available on StackLans and StackLanIPv6 for the moment.

.. code:: python

  def some_handler(flow):
      """ Some code on the flow """
      flow.reject = True

.. raw:: pdf

   PageBreak

External labeling
~~~~~~~~~~~~~~~~~
On some cases, the customer may want to label the communication with a personalized label, depending their needs. The system allows to label any Flow in order to label traffic as customer wants in a easy way.

.. code:: python

  def callback_for_http(flow):
    """ Call to some external service to verify the reputation of a domain """
    h = flow.http_info
    flow.label = external_domain_service(h.host_name)

Services as IP reputation, Domain reputation, GeoIP services could be used and label depending their return value.

.. raw:: pdf

   PageBreak

Data integration
~~~~~~~~~~~~~~~~
One of the biggest challenges of the engine is to allows to send the information to any type of database system. Nowadays, systems like 
`MySQL`_, `Redis`_, `Cassandra`_, `Hadoop`_ are on top of any company. By using the functionality of the DatabaseAdaptors, any integration could be possible with a negligible integration time.

For support multiple data destination we just need to generate a class and define the next methods:

* insert. This method will be called when a new UDP or TCP connection will be created.
* update. This method is called for update the information of the connection, and also when some important event happens.
* remove. This method is when the connection closes or dies by timeout.

For more information about adaptors, see `Database integration`_ . 

The information given on the update method is encode on JSON, but in some specific cases the system could generate MSGPack.

So just choose or write your adaptor and plugin to the stack as the example bellow

.. code:: python

    stack = pyaiengine.StackLan()

    stack.tcp_flows = 163840
    stack.udp_flows = 163840

    # Use your own adaptor (redisAdaptor, cassandraAdaptor, hadoopAdaptor, or whatever)
    db = redisAdaptor()
    db.connect("localhost")

    stack.set_udp_database_adaptor(db, 16)

    with pyaiengine.PacketDispatcher("eth0") as pdis:
        pdis.stack = stack
        pdis.run()

Here is the information that the engine provides on JSON format.

Bitcoin data
************

.. code:: json

  { 
    "bytes": 1664909,
    "info": {
        "blocks": 2,
        "rejects": 0,
        "tx": 6,
        "tcpflags": "Flg[S(1)SA(1)A(1662)F(0)R(0)P(8)Seq(1410785638,4110238515)]"
    },
    "ip": {
        "dst": "192.168.1.25",
        "src": "192.168.1.150"
    },
    "layer7": "BitcoinProtocol",
    "port": {
        "dst": 8333,
        "src": 55317 
    },
    "proto": 6
  }

CoAP data
*********

.. code:: json

  {
    "bytes": 233,
    "info": {
        "host": "someiot.com",
        "uri": "/some/resource/data/"
    },
    "ip": {
        "dst": "192.168.1.2",
        "src": "192.168.1.10"
    },
    "layer7": "CoAPProtocol",
    "port": {
        "dst": 5683,
        "src": 5531 
    },
    "proto": 17
  }

DCERPC data
***********

.. code:: json

  {
    "bytes": 2963,
    "info": {
        "tcpflags": "Flg[S(1)SA(1)A(14)F(0)R(0)P(9)Seq(3465082406,629632508)]",
        "uuid": "afa8bd80-7d8a-11c9-bef4-08002b102989"
    },
    "ip": {
        "dst": "192.168.3.43",
        "src": "10.0.2.15"
    },
    "layer7": "dcerpc",
    "port": {
        "dst": 49302,
        "src": 51296
    },
    "proto": 6
  }

DHCP data
*********

.. code:: json

  {
    "bytes": 300,
    "info": {
        "hostname": "EU-JOHN2"
    },
    "ip": {
        "dst": "255.255.255.255",
        "src": "192.168.3.3"
    },
    "layer7": "DHCPProtocol",
    "port": {
        "dst": 67,
        "src": 68 
    },
    "proto": 17
  }

DHCPv6 data
***********

.. code:: json

  {
    "bytes": 94,
    "info": {
        "hostname": "TSE-MANAGEMENT"
    },
    "ip": {
        "dst": "ff02::1:2",
        "src": "fe80::bc5a:f963:5832:fab"
    },
    "layer7": "dhcp6",
    "port": {
        "dst": 547,
        "src": 546
    },
    "proto": 17
  }


DNS data
********

.. code:: json

  {
    "bytes": 304,
    "info": {
        "dnsdomain": "youtube-ui.l.google.com",
        "ips": [
            "74.125.93.190",
            "74.125.93.136",
            "74.125.93.93",
            "74.125.93.91"
        ],
        "matchs": "Generic",
        "qtype": 1
    },
    "ip": {
        "dst": "198.164.30.2",
        "src": "192.168.5.122"
    },
    "layer7": "dns",
    "port": {
        "dst": 53,
        "src": 45428
    },
    "proto": 17
  }

DTLS data
*********

.. code:: json

  {
    "bytes": 429,
    "downstream_ttl": 0,
    "dtls": {
        "pdus": 0,
        "version": 65277
    },
    "evidence": false,
    "ip": {
        "dst": "2a03:39a0:1f:1004:b93c:3e15:d1e3:6848",
        "src": "2a03:39a0:1f:1000:38b6:67b7:3eea:fe28"
    },
    "layer7": "DTLS",
    "packets": 1,
    "port": {
        "dst": 49191,
        "src": 48809
    },
    "proto": 17,
    "reject": false,
    "upstream_ttl": 63
  }


HTTP data
*********

.. code:: json

  {
    "bytes": 9785,
    "info": {
        "ctype": "text/html",
        "host": "www.sactownroyalty.com",
        "reqs": 1,
        "ress": 1,
        "tcpflags": "Flg[S(1)SA(1)A(14)F(0)R(0)P(1)Seq(1008125706,1985601735)]"
    },
    "ip": {
        "dst": "74.63.40.21",
        "src": "192.168.4.120"
    },
    "layer7": "http",
    "port": {
        "dst": 80,
        "src": 3980
    },
    "proto": 6
  }

IMAP data
*********

.. code:: json

  {
    "bytes": 1708,
    "info": {
        "tcpflags": "Flg[S(1)SA(2)A(21)F(0)R(0)P(18)Seq(3603251617,2495559186)]",
        "user": "\"user11\""
    },
    "ip": {
        "dst": "192.168.5.122",
        "src": "192.168.2.111"
    },
    "layer7": "imap",
    "port": {
        "dst": 143,
        "src": 4479
    },
    "proto": 6,
    "reputation": "Suspicious"
  }

MQTT data
*********

.. code:: json

  {
    "bytes": 2509,
    "info": {
        "operation": 11,
        "total_client": 4,
        "total_server": 7,
        "tcpflags": "Flg[S(1)SA(1)A(22)F(1)R(0)P(10)Seq(2637347154,3369099113)]"
    },
    "ip": {
        "dst": "192.168.1.7",
        "src": "10.0.2.15"
    },
    "layer7": "MQTTProtocol",
    "port": {
        "dst": 1883,
        "src": 24479
    },
    "proto": 6
  }

Netbios data
************

.. code:: json

  {
    "bytes": 50,
    "info": {
        "netbiosname": "ISATAP"
    },
    "ip": {
        "dst": "192.168.100.7",
        "src": "192.168.100.201"
    },
    "layer7": "NetbiosProtocol",
    "port": {
        "dst": 137,
        "src": 137 
    },
    "proto": 17
  }


QUIC data
*********

.. code:: json

  {
    "bytes": 8284,
    "evidence": false,
    "ip": {
        "dst": "74.125.24.149",
        "src": "192.168.3.78"
    },
    "layer7": "quic",
    "port": {
        "dst": 443,
        "src": 60745
    },
    "proto": 17,
    "quic": {
        "host": "ad-emea.doubleclick.net",
        "ua": "Chrome/52.0.2743.116 Linux x86_64"
    }
  }

SSH data
********

.. code:: json

  {
    "bytes": 1853,
    "info": {
        "clientname": "SSH-2.0-Granados-2.0",
        "crypt_bytes": 0,
        "handshake": true,
        "servername": "SSH-2.0-OpenSSH_5.3p1 Debian-3ubuntu3",
        "tcpflags": "Flg[S(1)SA(1)A(10)F(0)R(0)P(6)Seq(1018474266,687901205)]"
    },
    "ip": {
        "dst": "192.168.5.122",
        "src": "192.168.79.190"
    },
    "layer7": "ssh",
    "port": {
        "dst": 22,
        "src": 60033
    },
    "proto": 6
  }

SSL data
********

.. code:: json

  {
    "bytes": 21831,
    "info": {
        "cipher": 47,
        "fingerprint": "1d095e68489d3c535297cd8dffb06cb9",
        "host": "fillizee.com",
        "issuer": "foror2",
        "pdus": 2,
        "tcpflags": "Flg[S(1)SA(1)A(30)F(0)R(0)P(5)Seq(1170091145,1113592977)]",
        "version": 769
    },
    "ip": {
        "dst": "10.0.0.254",
        "src": "10.0.0.1"
    },
    "layer7": "ssl",
    "port": {
        "dst": 443,
        "src": 49161
    },
    "proto": 6
  }

SMB data
********

.. code:: json

  {
    "bytes": 20506,
    "info": {
        "cmd": 17,
        "filename": "WP_SMBPlugin.pdf",
        "tcpflags": "Flg[S(1)SA(1)A(46)F(0)R(0)P(34)Seq(2608748647,3370812586)]"
    },
    "ip": {
        "dst": "10.0.0.12",
        "src": "10.0.0.11"
    },
    "layer7": "smb",
    "port": {
        "dst": 445,
        "src": 49208
    },
    "proto": 6
  }


SMTP data
*********

.. code:: json

  {
    "bytes": 412,
    "country": "Afganistan",
    "reputation": "Suspicious",
    "info": {
        "bytes": 0,
        "from": "TESTBED08@somelab.com",
        "tcpflags": "Flg[S(1)SA(2)A(13)F(0)R(0)P(9)Seq(2151667649,1152325196)]",
        "to": "testbed24@gmail.com",
        "total": 0
    },
    "ip": {
        "dst": "192.168.5.122",
        "src": "192.168.2.108"
    },
    "layer7": "smtp",
    "port": {
        "dst": 25,
        "src": 3431
    },
    "proto": 6,
    "timestamp": "2015-01-07 10:08:45.453259"
  }

SIP data
********

.. code:: json

  {
    "bytes":7100,
    "info": {
        "uri": "sip:192.168.1.200:5060;transport=UDP",
        "from": "'David Power'<sip:david_and@192.168.1.200:5060;transport=UDP>",
        "to":"'David Power'<sip:david_and@192.168.1.200:5060;transport=UDP>",
        "via":"SIP/2.0/UDP 192.168.1.100:5060"
        "voip": {
            "ip": {
                "dst": "192.168.100.140",
                "src": "192.168.1.1"
            },
            "port": {
                "dst": 64508,
                "src": 18874
            }
     },
    "ip": {
        "dst": "192.168.1.254",
        "src": "192.168.1.1"
    },
    "layer7": "SIPProtocol",
    "port": {
        "dst": 5060,
        "src": 23431
    },
    "proto": 17
  }

SSDP data
*********

.. code:: json

  {
    "bytes": 133,
    "info": {
        "host": "39.255.255.250:1900",
        "reqs": 1,
        "ress": 0,
        "uri": "*"
    },
    "ip": {
        "dst": "239.255.255.250",
        "src": "192.168.1.101"
    },
    "layer7": "ssdp",
    "port": {
        "dst": 1900,
        "src": 3277
    },
    "proto": 17
  }

POP data
********

.. code:: json

  {
    "bytes": 126,
    "info": {
        "tcpflags": "Flg[S(1)SA(2)A(13)F(0)R(0)P(10)Seq(3450492591,2097902556)]",
        "user": "user12"
    },
    "ip": {
        "dst": "192.168.5.122",
        "src": "192.168.2.112"
    },
    "layer7": "pop",
    "port": {
        "dst": 110,
        "src": 3739
    },
    "proto": 6
  }


.. raw:: pdf

   PageBreak

ZeroDay exploits signature generation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some exploits have the capability of encrypt their content for every instance, this is called Polymorphic/Metamorphism. On this case the generation of the signature depends on the speed of the vendor teams, and sometimes is late. For this case, the engine is capable of auto generate signatures of unknown traffic that will detect and neutralize (if integrate with a firewall) the attack.

This generation could be implemented by using the Python/Ruby API or by using the binary with combination of the network forensics functionality.

Nowadays, unknown attacks on any type of device happens, mobile phones, laptops, IoT devices and so on are perfect target for this attacks. By using the signature generation is possible for the customer to:

* Identify unknown network traffic sources.
* Generate evidences for a forensic analysis or storage.
* Given a pcap file of unknown traffic, identify automatically a valid signature for that traffic.
* Reuse the signature on real time and start to identify this unknown attack.

With this functionality customers don't depend on updates of third party companies, you owns your data.

.. raw:: pdf

   PageBreak

Yara signatures
~~~~~~~~~~~~~~~
The signatures generated by the system are of the customer, their data is important for them, and some signatures could be extremely value for some organizations for identify certain attacks. This signatures generated could be storage on Yara format in order to be compliant with other systems.

.. code:: python

  rule generated_by_ngnids_engine {
      meta:
         author="ngnids"
         description="Flows generated on port 1986"
         date="9/4/2015"
      strings:
         $a="^\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a"
      condition:
         $a
  }


.. raw:: pdf

   PageBreak

Network Forensics
~~~~~~~~~~~~~~~~~
In some cases there is a need for generate evidences of a receive attack or a specific network event. By using the EvidenceManager is possible to record specific network conversations on files for network forensic analysis. For use this functionality we just need to set the evidences property on the PacketDispatcher and on the network flow we want to track.

.. code:: python
  
  def some_handler(flow):
      """ Some code on the flow """
      flow.evidence = True

  with PacketDispatcher("eth0") as pdis:
        pdis.stack = stack
        pdis.evidences = True 
        pdis.run()

.. raw:: pdf

   PageBreak

Real time interaction 
~~~~~~~~~~~~~~~~~~~~~
The system have embedded a Lua/Ruby/Python interpreter similar as IPython. So is possible to interact by the user with the system without stooping the packet processing. This brings to the engine capabilities of inject any type of code, lua, ruby or python, on real time to the system without interrupting the service. Also the possibilities that brings to the user higher than traditional engines because there is direct interaction with the user on real time, no need to stops and starts daemon or services is needed.

For activate this functionality is just easy as set the variable enable_shell to true value.

.. code:: python

  with PacketDispatcher("eth0") as pdis:
        pdis.stack = stack
        # Enable the internal shell for interact with the engine
        pdis.enable_shell = True 
        pdis.run()

.. code:: lua

  pd:set_shell(true)
  pd:set_stack(st)
  pd:open("enp0s25") 
  pd:run()
  pd:close()

For more details, see `Injecting code on the engine`_ .

.. raw:: pdf

   PageBreak

Is possible to show the information of the network flows on real time and filter according to the user.

.. code:: python
   
   >>> stack.show_flows(l7protocol="dns")
   Flows on memory 31

   Flow                                                             Bytes      Packets    L7Protocol     Info
   Total 0

   Flow                                                             Bytes      Packets    L7Protocol     Info
   [192.168.0.101:34584]:17:[19.101.160.5:53]                       254        2          DNS            TTL(64,59) Domain:fedoraproject.org
   [192.168.0.101:38638]:17:[19.101.160.5:53]                       288        4          DNS            TTL(64,59) Domain:geolocation.onetrust.com
   [192.168.0.101:46078]:17:[19.101.160.5:53]                       288        4          DNS            TTL(64,59) Domain:geolocation.onetrust.com
   [192.168.0.101:34123]:17:[19.101.160.5:53]                       488        4          DNS            TTL(64,59) Domain:cdn.cookielaw.org
   [192.168.0.101:52922]:17:[19.101.160.5:53]                       238        2          DNS            TTL(64,59) Domain:cdn.cookielaw.org
   [192.168.0.101:41391]:17:[19.101.160.5:53]                       560        4          DNS            TTL(64,59) Domain:www.cisco.com
   [192.168.0.101:47773]:17:[19.101.160.5:53]                       560        4          DNS            TTL(64,59) Domain:www.cisco.com
   [192.168.0.101:35187]:17:[19.101.160.5:53]                       176        2          DNS            TTL(64,59) Domain:www.google.com
   [192.168.0.101:52179]:17:[19.101.160.5:53]                       374        2          DNS            TTL(64,59) Domain:incoming.telemetry.mozilla.org
   [192.168.0.101:50919]:17:[19.101.160.5:53]                       698        4          DNS            TTL(64,59) Domain:incoming.telemetry.mozilla.org
   [192.168.0.101:50022]:17:[19.101.160.5:53]                       188        2          DNS            TTL(64,59) Domain:collector-hpn.ghostery.net
   [192.168.0.101:44437]:17:[19.101.160.5:53]                       108        2          DNS            TTL(64,59) Domain:upload.wikimedia.org
   [192.168.0.101:43675]:17:[19.101.160.5:53]                       129        2          DNS            TTL(64,59) Domain:es.wikipedia.org
   Total 13


HTTP interface
~~~~~~~~~~~~~~

.. include:: httpinterface.rst

.. raw:: pdf

   PageBreak


Packet engines integration
~~~~~~~~~~~~~~~~~~~~~~~~~~
In some cases the engine needs to be integrated with a firewall or other packet engine. For this case the system allows to inject packets from other engines (Netfilter) to the system. By using this functionality, all the intelligence of the engine could be integrated in a firewall with the next simple steps

.. code:: python

  """ The dns_function have been attach to malware domains, so drop the traffic """
  def dns_function(flow):
      flow.accept = False

  def netfilter_callback(packet):

      payload = ethernet_header + packet.get_payload()
      length = packet.get_payload_len() + 14

      """ Use the forwardPacket method from the PacketDispatcher object
      in order to forward the packets from netfilter """
      pdis.forward_packet(payload,length)

      if (pdis.is_packet_accepted):
          packet.accept()
      else:
          packet.drop()

.. raw:: pdf

   PageBreak


Netfilter integration
~~~~~~~~~~~~~~~~~~~~~
This is only available on Linux systems. Is possible to read packets from a netfilter queue defined with iptables.
For use this mode you should run the creation and destroy of the netfitler queues via iptbles command, like the next example:

.. code:: bash

  # Creation of the queues
  iptables -I INPUT -i lo -p tcp -j NFQUEUE --queue-num 1
  ...
  # Destroy of the queues
  iptables -I INPUT -i lo -p tcp -j NFQUEUE --queue-num 1

For read the packets on the PacketDispatcher we just need to use the keyword netfilter plus the number of queue that we want to read the packets

.. code:: python

  # Read from netfilter on queue number 1
  with pyaiengine.PacketDispatcher("netfilter1") as pdis:
      pdis.stack = stack
      pdis.run()


.. raw:: pdf

   PageBreak


Network anomalies
~~~~~~~~~~~~~~~~~
Some attacks are very dependent of the protocol in use. Incorrect offset of headers, no headers on request, invalid URL formats and so on are present on the network nowadays. The engine supports the following network anomalies attacks.

* IPv4 fragmentation.
* IPv6 fragmentation.
* IPv6 loop extension headers.
* TCP bad flags and incorrect offset headers.
* UDP incorrect offsets.
* DNS incorrect headers and long names.
* SMTP incorrect emails.
* IMAP incorrect emails.
* POP incorrect emails.
* SNMP malformed headers.
* SSL malformed headers.
* HTTP malformed URI and no headers.
* CoAP malformed headers.
* RTP malformed headers.
* MQTT malformed headers.
* Netbios bogus headers.
* DHCP bogus headers.
* SMB bogus headers.

.. code:: python

  def my_function_for_http(flow):
    print("HTTP Anomaly detected")
    """ Some extra code here """

  stack.set_anomaly_callback(my_function_for_http, "HTTPProtocol")

The example above shows how to generate make specific use of HTTP anomalies and take advantage and create new detection functions.

.. raw:: pdf

   PageBreak

JA3 TLS Finterprint support
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The system can generate JA3 TLS fingerprints (https://github.com/salesforce/ja3) and after you can use them for make the detection as you want.

Please check on the example folder for usage.

This option needs to be set on compilation time (--enable-ja3) and also have the openssl-devel libraries on the system.

.. raw:: pdf

   PageBreak


Performance with other engines
------------------------------

.. include:: performance.rst

.. raw:: pdf

   PageBreak

Performance with multicore systems
----------------------------------

.. include:: multicoresystems.rst

.. raw:: pdf

   PageBreak

Use cases and examples
----------------------

This section contains examples and use cases that may help you on yours.
If you have a use case that would be interesting for adding feel free.

.. include:: zeusmalware.rst

.. raw:: pdf

   PageBreak

.. include:: virtualcloudmalwaredetection.rst

.. raw:: pdf

   PageBreak

.. include:: databaseintegration.rst

.. raw:: pdf

   PageBreak

.. include:: realtimeprograming.rst

.. raw:: pdf

   PageBreak

.. include:: binaryextractinformation.rst

.. raw:: pdf

   PageBreak

.. include:: malwareanalisys1.rst

.. raw:: pdf

   PageBreak

.. include:: randomwalware.rst

.. raw:: pdf

   PageBreak

.. include:: metasploit.rst

.. raw:: pdf

   PageBreak

API
---

Class description
~~~~~~~~~~~~~~~~~

* **BitcoinInfo**

  * Properties

    * total_blocks. Get the total number of Bitcoin blocks on the Flow.
    * total_rejects. Get the total number of Bitcoin rejects on the Flow.
    * total_transactions. Get the total number of Bitcoin transactions of the Flow.

* **Cache**

  This class manages the internal allocated memory of different object types manage by a protocol.

  * Methods

    * create. Allocate items inside the Cache.
    * destroy. Free items inside the Cache.
    * reset. Reset the values of the total variables.
    * show. Shows the Cache object.

  * Properties

    * dynamic_allocated_memory. Gets/Sets if the memory is allocated dynamic or not.
    * total_acquires. Returns the total of number of acquires on the Cache.
    * total_fails. Returns the total number of fails on the Cache.
    * total_items. Returns the total number of items on the Cache object.
    * total_releases. Returns the total number of releases objects on the Cache.

* **CoAPInfo**
 
  * Properties

    *  host_name. Gets the CoAP Hostname if the Flow is CoAP.
    *  matched_domain_name. Gets the matched DomainName object.
    *  uri. Gets the CoAP URI if the Flow is CoAP.

* **DCERCPInfo**

  Class that stores information of DCERPC.

  * Properties

    * uuid. Returns the UUID of DCERPC Flow.

* **DHCPInfo**

  * Properties

    * host_name. Gets the DHCP hostname.

* **DNSInfo**
 
  * Properties

    *  __iter__. Iterate over the IP addresses returned on the query response.
    *  domain_name. Gets the DNS domain name.
    *  matched_domain_name. Gets the matched DomainName object.

* **DTLSInfo**

  Class that stores information of DTLS.

  * Properties

    * pdus. Gets the total number of encrypted PDUs.
    * version. Gets the DTLS version of the flow.

* **DatabaseAdaptor** Abstract class

  * Methods
 
    * insert. Method called when a new Flow is created.
    * update. Method called when the Flow is updating.
    * remove. Method called when the Flow is removed.

* **DomainName**

  Class that manages a domain and the behavior.

  * Properties

    * callback. Gets/Sets the callback of the domain.
    * expression. Gets the domain expression.
    * http_uri_regex_manager. Gets/Sets the RegexManager used on this DomainName for matching URIs (only works on HTTP).
    * http_uri_set. Gets/Sets the HTTPUriSet used on this DomainName (only works on HTTP).
    * matchs. Gets the total number of matches of the domain.
    * name. Gets the name of the domain.
    * regex_manager. Gets/Sets the HTTP RegexManager used on this DomainName (only works on HTTP).

* **DomainNameManager**

  Class that manages DomainsNames.

  * Methods

    * __len__. Return the total number of DomainName objects on the DomainNameManager.
    * add_domain_name. Adds a DomainName by using the name and the domain name to the DomainNameManager.
    * remove_domain_name. Removes a DomainName by name.
    * reset. Reset the statistics of the DomainNameManager.
    * show. Shows the DomainName objects
    * show_matched_domains. Shows the DomainName objects that have been matched.

  * Properties

    * name. Gets/Sets the name of the DomainNameManager object.

* **Flow**

  Class that keeps all the relevant information of a network flow.

  * Methods

    * detach. Detach the flow from the current protocol.

  * Properties

    * accept. Accepts or drops the packet if there is a external engine (Netfilter).
    * anomaly. Gets the attached anomaly of the Flow.
    * bitcoin_info. Gets a BitcoinInfo object if the Flow is Bitcoin.
    * bytes. Gets the total number of bytes.
    * coap_info. Gets a CoAPInfo object if the Flow is CoAP.
    * dcerpc_info. Gets a DCERPCInfo object if the Flow is DCERPC.
    * dhcp6_info. Gets a DHCPv6Info object if the Flow is DHCPv6.
    * dhcp_info. Gets a DHCPInfo object if the Flow is DHCPv4.
    * dns_info. Gets a DNSInfo object if the Flow is a DNS.
    * downstream_ttl. Returns the IP.TTL last packet of downstream.
    * dst_ip. Gets the destination IP address.
    * dst_port. Gets the destination port of the Flow.
    * dtls_info. Gets a DTLSInfo object if the Flow is DTLS.
    * duration. Gets the duration on secs of the Flow.
    * evidence. Gets/Sets the evidence of the Flow for make forensic analysis.
    * frequencies. Gets a map of frequencies of the payload of the Flow.
    * have_tag. Gets if the Flow have tag from lower network layers.
    * http_info. Gets the HTTPInfo if the Flow is HTTP.
    * imap_info. Gets the IMAP Info if the Flow is IMAP.
    * ip_set. Gets the IPSet Info of the Flow if is part of an IPSet.
    * l7protocol. Gets the name of the Protocol of L7 of the Flow.
    * label. Gets/Sets the label of the Flow (external labeling).
    * mqtt_info. Gets a MQTTInfo object if the Flow is MQTT.
    * netbios_info. Gets a NetbiosInfo object if the Flow is Netbios.
    * packet_frequencies. Gets the packet frequencies of the Flow.
    * packets. Gets the total number of packets on the Flow.
    * packets_layer7. Gets the total number of layer7 packets.
    * payload. Gets a list of the bytes of the payload of the Flow.
    * pop_info. Gets the POP Info if the Flow is POP.
    * protocol. Gets the protocol of the Flow (tcp,udp).
    * quic_info. Gets the QuicInfo object if the Flow is Google Quic.
    * regex. Gets the regex if the Flow have been matched with the associated regex.
    * regex_manager. Gets/Sets the RegexManager.
    * reject. Gets/Sets the reject of the connection.
    * sip_info. Gets the SIPInfo if the Flow is SIP.
    * smb_info. Gets a SMBInfo object if the Flow is Samba.
    * smtp_info. Gets the SMTP Info if the Flow is SMTP.
    * src_ip. Gets the source IP address.
    * src_port. Gets the source port of the Flow.
    * ssdp_info. Gets a SSDPInfo object if the Flow is SSDP.
    * ssh_info. Gets a SSHInfo object if the Flow is SSH.
    * ssl_info. Gets a SSLInfo object the Flow is SSL.
    * tag. Gets the tag from lower network layers.
    * tcp_info. Gets a TCPInfo object if the Flow is TCP.
    * upstream_ttl. Returns the IP.TTL last packet of upstream.

* **FlowManager**

  This class stores in memory the active Flows.

  * Methods

    * __iter__. Iterate over the Flows stored on the FlowManager object.
    * __len__. Gets the number of Flows stored on the FlowManager.
    * flush. Retrieve the active flows to their correspondig caches and free the flow resources.
    * show. Shows the active flows on memory.

  * Properties

    * flows. Gets the number of Flows stored on the FlowManager.
    * process_flows. Gets the total number of process Flows.
    * timeout. Gets/Sets the flows timeout.
    * timeout_flows. Gets the total number of Flows that have been expired by the timeout.

* **HTTPInfo**

  Class that stores information of HTTP.

  * Properties

    * banned. Gets/Sets the Flow banned for no more analysis on the python side and release resources.
    * content_type. Gets the HTTP Content Type if the Flow is HTTP.
    * host_name. Gets the HTTP Host if the Flow is HTTP.
    * matched_domain_name. Gets the matched DomainName object.
    * uri. Gets the HTTP URI if the Flow is HTTP.
    * user_agent. Gets the HTTP UserAgent if the Flow is HTTP.

* **HTTPUriSet**

  * Properties

    * callback. Gets/Sets a callback function for the matching set.
    * lookups. Gets the total number of lookups of the set.
    * lookups_in. Gets the total number of matched lookups of the set.
    * lookups_out. Gets the total number of non matched lookups of the set.
    * uris. Gets the total number of URIs on the set. (__LEN__) TODO

  * Methods

    * add_uri. Adds a URI to the HTTPUriSet.

* **IMAPInfo**

  * Properties

    * user_name. Gets the user name of the IMAP session if the Flow is IMAP.

* **IPSet**

  Class that stores and manages IP addresses on a set.

  * Methods

    * __len__. Returns the total number of IP address on the IPSet.
    * add_ip_address. Add a IP address to the IPSet.
    * remove_ip_address. Removes a IP address from the IPSet.
    * show. Shows the IP addresses of the IPSet.


  * Properties

    * callback. Gets/Sets a function callback for the IPSet.
    * lookups. Gets the total number of lookups of the IPSet.
    * lookups_in. Gets the total number of matched lookups of the IPSet.
    * lookups_out. Gets the total number of non matched lookups of the IPSet.
    * name. Gets the name of the IPSet.
    * regex_manager. Gets/Sets the RegexManager for this group of IP addresses.

* **IPSetManager**

  Class that stores and manages IPSets, IPRadixTrees and IPBloomSets.

  * Methods

    * add_ip_set. Adds a IPSet.
    * remove_ip_set. Removes a IPSet by the reference.
    * reset. Reset the statistics of the IPSetManager object.
    * show. Shows the IPSets.

  * Properties

    * __iter__. Iterate over the IPSets.
    * __len__. Return the total number of IPSets.
    * name. Gets/Sets the name of the IPSetManager object.

* **MQTTInfo**

  * Properties

    * topic. Gets the MQTT publish topic if the Flow is MQTT.

* **NetbiosInfo**

  * Properties

    * name. Gets the Netbios Name.

* **NetworkStack**

  Abstract class that implements a common network stack.

  * Methods

    * attach_to. Attach a flow Object to a given protocol.
    * decrease_allocated_memory. Decrease the allocated memory for a protocol given as parameter.
    * disable_protocol. Disable the protocol from the stack.
    * enable_protocol. Enable the protocol on the stack.
    * get_cache. Gets the internal Cache objet by protocol and name.
    * get_cache_data. 
    * get_counters. Gets the counters of a specific protocol on a python dict.
    * increase_allocated_memory. Increase the allocated memory for a protocol given as parameter.
    * release_cache. Release the cache of a specific protocol.
    * release_caches. Release all the caches.
    * reset_counters. Reset the values of the protocol counters.
    * set_anomaly_callback. 
    * set_domain_name_manager. Sets a DomainNameManager on a specific protocol (HTTP,SSL or DNS).
    * set_dynamic_allocated_memory. 
    * set_tcp_database_adaptor. 
    * set_udp_database_adaptor. 
    * show. Shows the statistics of the stack.
    * show_anomalies. Shows the anomalies of the traffic.
    * show_flows. Shows the active flows on memory.
    * show_protocol_statistics. 

* **POPInfo**

  * Properties

    * user_name. Gets the user name of the POP session if the Flow is POP.

* **PacketDispatcher**

  Class that manage the packets and forwards to the associated network stack

  * Methods

    * add_timer. Sets a timer for manage periodically tasks (DDoS checks, abuse, etc...).
    * close. Closes a network device or a pcap file.
    * forward_packet. Forwards the received packet to a external packet engine(Netfilter).
    * open. Opens a network device or a pcap file for analysis.
    * remove_timer. Removes a timer.
    * run. Start to process packets.
    * show. Shows the current statistics.
    * show_current_packet. Shows the current packet that is been processed.
    * show_system. Shows the system statistics of the running process.

  * Properties

    * authorized_ip_address. List of IP address that are authorized to connect the HTTP interface.
    * bytes. Gets the total number of bytes process by the PacketDispatcher.
    * enable_shell. Gets/Sets a python shell in order to interact with the system on real time.
    * evidences. Gets/Sets the evidences for make forensic analysis.
    * http_port. Gets/Sets the HTTP port for listening incoming connections.
    * is_packet_accepted. Returns if the packet should be accepted or not (for integration with Netfilter).
    * log_user_commands. Enables or disable the generation of user command line log files.
    * packets. Gets the total number of packets process by the PacketDispatcher.
    * pcap_filter. Gets/Sets a pcap filter on the PacketDispatcher
    * stack. Gets/Sets the Network stack that is running on the PacketDispatcher.
    * status. Gets the status of the PacketDispatcher.

* **Regex**

  This class contains the functionality for manage regular expressions as well as how to connect the object with others.

  * Properties

    * callback. Gets/Sets the callback function for the regular expression.
    * expression. Gets the regular expression.
    * matchs. Gets the number of matches of the regular expression.
    * name. Gets the name of the regular expression.
    * next_regex. Gets/Sets the next regular expression that should match.
    * next_regex_manager. Gets/Sets the next RegexManager for assign to the Flow when a match occurs.
    * write_packet. Forces to write the payload that matchs the Regex on a DatabaseAdaptor object.

* **RegexManager**

  This class contains Regex objects and how are they manage.

  * Methods

    * __len__. Gets the total number of Regex stored on the RegexManager object.
    * __iter__. Iterate over the Regex stored on the RegexManager object.
    * add_regex. Adds a Regex object to the RegexManager.
    * remove_regex. Removes one or multiple Regexs objects from the RegexManager.
    * reset. Resets the values of the statistics of matches.
    * show. Shows the Regexs stored on the RegexManager.
    * show_matched_regexs. Shows the Regexs that have been matched.

  * Properties

    * callback. Gets/Sets the callback function for the RegexManager for regular expressions that matches.
    * name. Gets/Sets the name of the RegexManager.

* **SIPInfo**

  * Properties

    * from_name. Gets the SIP From if the Flow is SIP.
    * to_name. Gets the SIP To if the Flow is SIP.
    * uri. Gets the SIP URI if the Flow is SIP.
    * via. Gets the SIP Via if the Flow is SIP.

* **SMBInfo**

  Class that stores information of SMB.

  * Properties

    * filename. Gets the filename from the SMBInfo object.

* **SMTPInfo**

  * Properties

    * banned. Gets or Sets the banned of the Flow.
    * mail_from. Gets the Mail From if the Flow is SMTP.
    * mail_to. Gets the Rcpt To if the Flow is SMTP. 

* **SSDPInfo**

  * Properties

    * host_name. Gets the SSDP Host if the Flow is SSDP.
    * uri. Gets the SSDP URI if the Flow is SSDP.

* **SSHInfo**

  Class that stores information of SSH.

  * Properties

    * client_name. Returns the name of the SSH client agent.
    * encrypted_bytes. Returns the number of encrypted bytes of the Flow.
    * server_name. Returns the name of the SSH server agent.

* **SSLInfo**

  Class that stores information of SSL.

  * Properties

    * cipher. Returns the identifier of the Cipher used.
    * issuer_name. Gets the SSL Issuer common name.
    * matched_domain_name. Gets the matched DomainName object.
    * server_name. Gets the SSL server name.
    * session_id. Gets the TLS session id of the connection if exists.
    * fingerprint. Gets the TLS fingerprint of the object.

* **Stack<Lan/LanIPv6/Mobile/Virtual/OpenFlow/MobileIPv6>**

  Class that implements a network stack

  * Methods

    * attach_to. Attach a flow Object to a given protocol.
    * decrease_allocated_memory. Decrease the allocated memory for a protocol given as parameter.
    * disable_protocol. Disable the protocol from the stack.
    * enable_protocol. Enable the protocol on the stack.
    * get_cache. Gets the internal Cache objet by protocol and name.
    * get_cache_data. Gets the data of a cache and protocol on a dict object.
    * get_counters. Gets the counters of a specific protocol on a python dict.
    * increase_allocated_memory. Increase the allocated memory for a protocol given as parameter.
    * release_cache. Release the cache of a specific protocol.
    * release_caches. Release all the caches.
    * reset_counters. Reset the values of the protocol counters.
    * set_anomaly_callback. Sets a callback for specific anomalies on the given protocol.
    * set_domain_name_manager. Sets a DomainNameManager on a specific protocol (HTTP,SSL or DNS).
    * set_dynamic_allocated_memory. 
    * set_tcp_database_adaptor. Sets a databaseAdaptor for TCP traffic.
    * set_udp_database_adaptor. Sets a databaseAdattor for UDP traffic.
    * show. Shows the statistics of the stack.
    * show_anomalies. Shows the anomalies of the traffic.
    * show_flows. Shows the active flows on memory.
    * show_protocol_statistics. 

  * Properties

    * flows_timeout. Gets/Sets the timeout for the TCP/UDP Flows of the stack
    * link_layer_tag. Gets/Sets the Link layer tag for Vlans,Mpls encapsulations.
    * mode. Sets the operation mode of the Stack (full, frequency, nids).
    * name. Gets the name of the Stack.
    * stats_level. Gets/Sets the number of statistics level for the stack (1-5).
    * tcp_flow_manager. Gets the TCP FlowManager for iterate over the Flows.
    * tcp_flows. Gets/Sets the maximum number of Flows to be on the cache for TCP traffic.
    * tcp_ip_set_manager. Gets/Sets the TCP IPSetManager for TCP traffic.
    * tcp_regex_manager. Gets/Sets the TCP RegexManager for TCP traffic.
    * udp_flow_manager. Gets the UDP FlowManager for iterate over the Flows.
    * udp_flows. Gets/Sets the maximum number of Flows to be on the cache for UDP traffic.
    * udp_ip_set_manager. Gets/Sets the UDP IPSetManager for UDP traffic.
    * udp_regex_manager. Gets/Sets the UDP RegexManager for UDP traffic.

* **TCPInfo**

  Class that stores information of TCP.

  * Properties

    * acks. Return the total number of TCP ack packets of the Flow.
    * fins. Return the total number of TCP fin packets of the Flow.
    * pushs. Return the total number of TCP push packets of the Flow.
    * rsts. Return the total number of TCP rst packets of the Flow.
    * state. Return the state of the TCP Flow.
    * synacks. Return the total number of TCP syn/ack packets of the Flow.
    * syns. Return the total number of TCP syn packets of the Flow.

.. raw:: pdf

   PageBreak

References
----------

.. _MySql: https:://www.mysql.com
.. _Redis: http://redis.io 
.. _Cassandra: https://cassandra.apache.org/
.. _Hadoop: https://hadoop.apache.org/

.. raw:: pdf

   PageBreak

Terms and conditions
--------------------

AIEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

AIEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AIEngine.  If not, see <http://www.gnu.org/licenses/>.

