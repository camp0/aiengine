The engine allows to load an HTTP server for configuration and retrieve information

If you decide to use the binary is the -a parameter

.. code:: bash 

   -a [ --port ] arg (=0)             Sets the HTTP listenting port.


Or if you want to decide to use PacketDispatcher object of the python binding use:

.. code:: python

    pd.http_address = "192.168.0.1"
    pd.http_port = 5008
    pd.authorized_ip_address = ["127.0.0.1", "172.17.0.0/24"]

This allows to access to one running instance and interact and reprogram over an HTTP interface.

The engine dont have support for TLS connections, if you need to securely the HTTP please consider user SSH tunnels for that.

The available URIs on the server are:

- /v1/protocols/summary
- /v1/protocol
- /v1/flows
- /v1/summary
- /v1/system
- /v1/uris
- /v1/pcapfile
- /v1/python_code
- /v1/flow
- /v1/locals
- /v1/current_packet
- /v1/logs
- /v1/health
- /v1/anomalies
- /v1/help
- /v1/evidences.<pid>.pcap
- /v1/current_code
- /v1/alarms

/v1/uris
********

This uri contains the available uris that the HTTP server provides.

.. code:: bash

   GET /v1/uris HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/html
   Content-Length: 720

   <html><head><title>AIEngine operations</title></head>
   <body>
   <a href="http://127.0.0.1:5008/v1/protocols/summary">Protocols summary</a><br>
   <a href="http://127.0.0.1:5008/v1/protocol">Protocol summary</a><br>
   <a href="http://127.0.0.1:5008/v1/flows">Network flows</a><br>
   <a href="http://127.0.0.1:5008/v1/summary">Summary</a><br>
   <a href="http://127.0.0.1:5008/v1/system">System</a><br>
   <a href="http://127.0.0.1:5008/v1/pcapfile">Upload pcapfile</a><br>
   <a href="http://127.0.0.1:5008/v1/python_code">Python code</a><br>
   <a href="http://127.0.0.1:5008/v1/locals">Python locals</a><br>
   <a href="http://127.0.0.1:5008/v1/flow">Network flow</a><br>
   <a href="http://127.0.0.1:5008/v1/current_packet">Current packet</a><br>
   <a href="http://127.0.0.1:5008/v1/logs">Logs</a><br>
   </body>
   </html>


/v1/protocols/summary
*********************

.. code:: bash

   GET /v1/protocols/summary HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 3899

   Protocol statistics summary
   ...

.. code:: bash

   GET /v1/protocols/summary HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: application/json
   Content-Length: 3054

   [
      {"bytes":655,"cache_memory":0,"events":0,"memory":968,"miss":0,"name":"Ethernet","packets":4,"used_memory":968},
      {"bytes":599,"cache_memory":0,"events":0,"memory":984,"miss":0,"name":"IP","packets":4,"used_memory":984},
      {"bytes":0,"cache_memory":0,"events":0,"memory":689216,"miss":0,"name":"TCP","packets":0,"used_memory":1088},
      {"bytes":519,"cache_memory":0,"events":0,"memory":287776,"miss":0,"name":"UDP","packets":4,"used_memory":1336},
   ...


/v1/flow
********

The user can retrieve information about a specific TCP/UDP flow and also
modify some of the attributes while the engine is running.

The available parameters are:

- id -> Flow identifier
- tag -> vlan, vxlan identifier
- payload -> enables for see the L7 payload of the flow
- len -> Shows the number of bytes of the L7 payload


.. code:: bash

   GET /v1/flow?id=[192.168.1.1:63139]:17:[192.168.1.254:53] HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: application/json
   Content-Length: 178

   {"bytes":40,
    "dns":{"domain":"s2.youtube.com","qtype":1},
    "evidence":false,
    "ip":{"dst":"192.168.1.254","src":"192.168.1.1"},
    "layer7":"dns",
    "port":{"dst":53,"src":63139},
    "proto":17}


Also modify some of the fields of the network flow

.. code:: bash

   PUT /v1/flow?id=[192.168.1.1:63139]:17:[192.168.1.254:53] HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Length: 45
   Content-Type: application/json

   {"label": "This is a lovely label my friend"}

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Length: 0

Is also possible that on some cases we want to see the information that is inside the flow.
For achive that we just need to set the variable payload to true. This wil copy the application layer information
in the flow, so the first time we send the request we wont see anything on the output, just the next ones.

.. code:: bash

   $ curl -g "http://192.168.0.220:8080/v1/flow?id=[123.166.202.251:23711]:6:[192.168.0.220:8444]&payload=true"
   Network flow: 123.166.202.251:23711:6:192.168.0.220:8444
	Protocol:                      TCP
	L7Protocol:                    SSL
	Accept:                        yes
	Evidence:                       no
	Reject:                         no
	Anomaly:                      None
	Status:                     active
	Up bytes:                    73409
	Down bytes:                 293395
	Up packets:                   1200
	Down packets:                 1400
	Duration:              00:00:28:36
	Last packet seen:           6 secs
	IP ttl:                   (114,64)
	TCP state:             ESTABLISHED
	TCP: Flg[S(1)SA(1)A(2598)F(0)R(0)P(1279)Seq(3941251426,1073817884)WS(1024,846)]
	Layer7: Pdus:1289 Ver:0x301

.. code:: bash

  $ curl -g "http://192.168.0.220:8080/v1/flow?id=[123.166.202.251:23711]:6:[192.168.0.220:8444]&payload=true"
  Network flow: 123.166.202.251:23711:6:192.168.0.220:8444
	Protocol:                      TCP
	L7Protocol:                    SSL
	Accept:                        yes
	Evidence:                       no
	Reject:                         no
	Anomaly:                      None
	Status:                     active
	Up bytes:                    73541
	Down bytes:                 293641
	Up packets:                   1203
	Down packets:                 1403
	Duration:              00:00:28:45
	Last packet seen:           0 secs
	IP ttl:                   (114,64)
	TCP state:             ESTABLISHED
	TCP: Flg[S(1)SA(1)A(2604)F(0)R(0)P(1282)Seq(3941251558,1073818130)WS(1026,846)]
	Layer7: Pdus:1292 Ver:0x301

	17 03 03 00 71 fe 51 f3 47 99 e6 a5 af 7c 01 b5          ....q.Q.G....|..
	10 d3 fe 79 75 a3 3f 03 d1 b9 66 39 4e 0d c4 bd          ...yu.?...f9N...
	a0 12 b8 e3 59 4f 2c 73 24 9c 8b a2 77 a7 72 7b          ....YO,s$...w.r{
	0a 50 20 14 2e 66 15 05 da 1b ab a8 fa 5e 19 fa          .P ..f.......^..
	8a 94 19 08 df 31 f4 70 18 e0 33 cb ea 63 f2 13          .....1.p..3..c..
	dd c3 28 a3 1a 4f 74 de b5 c7 da 06 2b b1 f3 66          ..(..Ot.....+..f
	0d ec f9 77 33 39 7d 3b b2 16 cc 81 6a b7 49 89          ...w39};....j.I.
	7a f6 a0 29 7a 8f                                        z..)z



/v1/flows
*********

Is possible to query the flows by the following parameters:

- src_port -> Source Port 
- dst_port -> Destination Port
- src_ip -> Source IP address/network
- dst_ip -> Destination IP address/networkp
- protocol -> Protocol TCP(6) or UDP(17)
- limit -> Limit the number of flows show
- l7protocol -> Layer7 protocol name (dns, http, and so on)

.. code:: bash

   GET /v1/flows?limit=100 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1


.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 380

   Flows on memory 1

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   [10.0.2.15:51413]:17:[88.190.242.141:6881]                       519        4          UDPGenericProtocol
   Total 1


You can use the Layer7 protocol name on the URI and filter them

.. code:: bash

   GET /v1/flows&l7protocol=http&stats_level=1 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 274

   Flows on memory 1

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

/v1/protocol
************

This URI have the next parameters available:

- name -> Name of the protocol to display
- map -> Name of the internal memory data map
- stats_level -> Level of stats detail

.. code:: bash

   GET /v1/protocol?name=dns HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 184

   DNSProtocol(0x5611c9823cf0) statistics
        Dynamic memory alloc:           no
        Total allocated:         73 KBytes
        Total packets:                   0
	Total bytes:                     0


.. code:: bash

   GET /v1/protocol?name=dns&stats_level=5 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 1656

   DNSProtocol(0x5611c9823cf0) statistics
	Dynamic memory alloc:           no
	Total allocated:         73 KBytes
	Total packets:                   0
	Total bytes:                     0
	Total valid packets:             0
	Total invalid packets:           1
	Total allow queries:             0
	Total banned queries:            0
	Total queries:                   0
	Total responses:                 0
	Total type A:                    0
	Total type NS:                   0
	Total type CNAME:                0
	Total type SOA:                  0
	Total type PTR:                  0
	Total type MX:                   0
	Total type TXT:                  0
	Total type AAAA:                 0
	Total type LOC:                  0
	Total type SRV:                  0
	Total type DS:                   0
	Total type SSHFP:                0
	Total type DNSKEY:               0
	Total type IXFR:                 0
	Total type ANY:                  0
	Total type others:               0
   FlowForwarder(0x5611c97113b0) statistics
	Plugged to object(0x5611c9823cf0)
	Total forward flows:             0
	Total received flows:            0
	Total fail flows:                0
   DNS Info cache statistics
	Total items:                   512
	Total allocated:         44 KBytes
	Total current alloc:     44 KBytes
	Total acquires:                  0
	Total releases:                  0
	Total fails:                     0
   Name cache statistics
	Total items:                   512
	Total allocated:         28 KBytes
	Total current alloc:     28 KBytes
	Total acquires:                  0
	Total releases:                  0
	Total fails:                     0
	DNS Name usage

.. code:: bash

   GET /v1/protocol?name=dns&stats_level=5 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: application/json
   Content-Length: 337

   {"allocated_bytes":75056,
    "allow_queries":0,
    "banned_queries":0,
    "bytes":0,
    "dynamic_memory":false,
    "invalid_packets":1,
    "name":"DNSProtocol",
    "packets":0,
    "queries":0,
    "responses":0,
    "types":{"a":0,
        "aaaa":0,
        "any":0,
        "cname":0,
        "dnskey":0,
        "ds":0,
        "ixfr":0,
        "loc":0,
        "mx":0,
        "ns":0,
        "others":0,
        "ptr":0,
        "soa":0,
        "srv":0,
        "sshfp":0,
        "txt":0},
    "valid_packets":0}

.. code:: bash

   GET /v1/protocol?name=http&map=hosts HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: application/json
   Content-Length: 20

   {"www.google.com":1}


/v1/system
**********

.. code:: bash

   GET /v1/system HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: application/json
   Content-Length: 316

   {"elapsed_time":"00:00:07.355174",
    "lock_memory":false,
    "machine":"x86_64",
    "nodename":"vmfedora25",
    "pid":10865,
    "release":"5.0.16-100.fc28.x86_64",
    "resident_memory":26768,
    "shared_memory":0,
    "sysname":"Linux",
    "unshared_data":0,
    "unshared_stack":0,
    "version":"#1 SMP Tue May 14 18:22:28 UTC 2019",
    "virtual_memory":411856896}

/v1/pcapfile
************

Now is possible to upload pcap files to the engine for analisys

.. code:: bash

   POST /v1/pcapfile HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Length: 3323
   Content-Type: multipart/form-data; boundary=80ff982a95a2aa44cfd13c2b9ac390e9

   --80ff982a95a2aa44cfd13c2b9ac390e9
   Content-Disposition: form-data; name="file"; filename="accessgoogle.pcap"

   ........................q=.Q.q..J...J...$v}9.q...IC\..E..<^.@.@. ....
   Ye.....5.(AI.............www.google.com.....q=.Q.q..J...J...$v}9.q...IC\..E..<^.@.@. ....
   Ye.....5.(...............www.google.com.....q=.Q...............IC\$v}9.q..E.....@.;..1Ye.....
   ...

/v1/python_code
***************

Is possible to send python code directly to the engine in order to modify the behavior


.. code:: bash

   POST /v1/python_code HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Type: text/python
   Content-Length: 18

   a = 1 + 5
   print(a)

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Length: 2

   6

Sometimes we would like to restrict the people that can inject code on the system. For achive this
we will use the parameter authorization_code_token from the PacketDispatcher.


.. code:: bash

   [luis@localhost src]$ python
   Python 3.9.9 (main, Nov 19 2021, 00:00:00)
   [GCC 10.3.1 20210422 (Red Hat 10.3.1-1)] on linux
   Type "help", "copyright", "credits" or "license" for more information.
   >>> import pyaiengine
   >>> import base64
   >>> pdis = pyaiengine.PacketDispatcher()
   >>> pdis.authorization_code_token = base64.b64encode(b"AAAAAAAAAA:")
   >>> print(pdis.authorization_code_token)
   QUFB...
   >>> pdis.show()
   PacketDispatcher statistics
	Connected to
	Total process packets:           0
	Total bytes:                     0
	Shell:                    disabled
	Total TCP server connections:    0
	Authorization code:        QUFB...
   >>>

.. code:: bash

   POST /v1/python_code HTTP/1.1
   Host: localhost:8080
   User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:99.0) Gecko/20100100 Firefox/97.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: en-US,en;q=0.5
   Accept-Encoding: gzip, deflate, br
   Content-Type: multipart/form-data; boundary=---------------------------379431751519787882393374753053
   Content-Length: 185
   Origin: http://localhost:8080
   DNT: 1
   Connection: keep-alive
   Referer: http://localhost:8080/v1/python_code
   Upgrade-Insecure-Requests: 1

   -----------------------------379431751519787882393374753053
   Content-Disposition: form-data; name="code"

   print("Hi!")
   -----------------------------379431751519787882393374753053--

.. code:: bash

   HTTP/1.1 401 Unauthorized
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 31
   WWW-Authenticate: Basic realm='AIEngine'

   Wrong authorization credentials

.. code:: bash

   POST /v1/python_code HTTP/1.1
   Host: localhost:8080
   User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:99.0) Gecko/20100100 Firefox/97.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: en-US,en;q=0.5
   Accept-Encoding: gzip, deflate, br
   Content-Type: multipart/form-data; boundary=---------------------------379431751519787882393374753053
   Content-Length: 185
   Origin: http://localhost:8080
   DNT: 1
   Connection: keep-alive
   Referer: http://localhost:8080/v1/python_code
   Upgrade-Insecure-Requests: 1
   Authorization: Basic QUFBQUFBQUFBQTo=

   -----------------------------379431751519787882393374753053
   Content-Disposition: form-data; name="code"

   print("Hi!")
   -----------------------------379431751519787882393374753053--

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 36

   Python code loaded successfully
   Hi!



/v1/locals
**********

When the engine is running with the python binding is possible to retrieve the variables loaded on the server.
This allows the user to reprogram the instance as he wants depending on what have that instance loaded on memory.

.. code:: bash

   GET /v1/locals HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
   Accept-Language: en-US,en;q=0.5
   Accept-Encoding: gzip, deflate
   Referer: http://127.0.0.1:8080/v1/uris
   Connection: keep-alive
   Upgrade-Insecure-Requests: 1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 200

   Python objects
	pcapfile:str
	__builtins__:module
	__file__:str
	__package__:NoneType
	sys:module
	pyaiengine:module
	pd:PacketDispatcher
	__name__:str
	rm:RegexManager
	st:StackLan
	__doc__:NoneType

Also is possible to get the attributes of any object instantiated

.. code:: bash

   GET /v1/locals?name=stack HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   DNT: 1
   Connection: keep-alive
   Upgrade-Insecure-Requests: 1
   Sec-GPC: 1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 395

   Object stack
	flows_timeout = 180
	link_layer_tag = 
	mode = full
	name = Lan network stack
	stats_level = 0
	tcp_flow_manager = <pyaiengine.FlowManager object at 0x7f9201b95340>
	tcp_flows = 262083
	tcp_ip_set_manager = None
	tcp_regex_manager = None
	udp_flow_manager = <pyaiengine.FlowManager object at 0x7f9201b95340>
	udp_flows = 131071
	udp_ip_set_manager = None
	udp_regex_manager = None

And is possible to reconfigure the engine and change the behavior. For example
the user wants to change a property of the stack object.

.. code:: bash

   GET /v1/locals?name=stack HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: python-requests/2.24.0
   Accept-Encoding: gzip, deflate
   Accept: */*
   Connection: keep-alive

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 395

.. code:: bash

   Object stack
	flows_timeout = 180
	link_layer_tag = 
	mode = full
	name = Lan network stack
	stats_level = 0
	tcp_flow_manager = <pyaiengine.FlowManager object at 0x7f4c42d54340>
	tcp_flows = 262061
	tcp_ip_set_manager = None
	tcp_regex_manager = None
	udp_flow_manager = <pyaiengine.FlowManager object at 0x7f4c42d54340>
	udp_flows = 131070
	udp_ip_set_manager = None
	udp_regex_manager = None

The user wants to reduce the time of for the flows to 100

.. code:: bash

   PUT /v1/locals?name=stack&property=flows_timeout&value=100 HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: python-requests/2.24.0
   Accept-Encoding: gzip, deflate
   Accept: */*
   Connection: keep-alive
   Content-Length: 0

.. code:: bash
 
   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Length: 0

And then verify that the change has been made

.. code:: bash

   GET /v1/locals?name=stack&property=flows_timeout HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: python-requests/2.24.0
   Accept-Encoding: gzip, deflate
   Accept: */*
   Connection: keep-alive

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.0.1
   Content-Type: text/plain
   Content-Length: 34

   Object stack
	flows_timeout = 100

/v1/current_packet
******************

Shows the current packet that is process at that moment.

.. code:: bash

   GET /v1/current_packet HTTP/1.1
   Host: 192.168.0.220:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   DNT: 1
   Connection: keep-alive
   Referer: http://192.168.0.220:8080/
   Upgrade-Insecure-Requests: 1
   Sec-GPC: 1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 407

	Flow:114.254.3.251:58956:6:192.168.0.220:8444
	54 67 51 e7 97 10 0c 7a 15 01 08 ce 08 00 45 00          TgQ....z......E.
	00 34 dd 57 40 00 40 06 24 ef c0 a8 00 dc 72 fe          .4.W@.@.$.....r.
	03 fb 20 fc e6 4c 7f 8e 99 8a 6b e8 45 77 80 10          .. ..L....k.Ew..
	30 0e 38 a4 00 00 01 01 08 0a 74 e3 ac 3f 33 eb          0.8.......t..?3.
	f1 32                                                    .

/v1/logs
********

Shows the last 16 entries of the current log file. The parameter limit controls the number of entries
that can be retrieved.


.. code:: bash

   GET /v1/logs HTTP/1.1
   Host: 192.168.0.220:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   Connection: keep-alive

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 954

   [2022-03-18 13:02:07] [info] GET /v1/flows text/plain 200
   [2022-03-18 16:19:03] [info] GET / text/html 200
   [2022-03-18 16:19:03] [info] GET /favicon.ico text/html 200
   [2022-03-18 16:19:11] [info] GET /v1/current_packet text/plain 200
   [2022-03-18 16:19:24] [info] GET /v1/current_packet text/plain 200


/v1/health
**********

Used for determine if the process is up and running by third party systems.

.. code:: bash

   GET /v1/health HTTP/1.1
   Host: 192.168.0.220:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   Connection: keep-alive

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 17

   Up and running ;)


/v1/anomalies
*************

Used for show the packet anomalies that the stack process.

.. code:: bash

   GET /v1/anomalies HTTP/1.1
   Host: 192.168.0.220:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   Connection: keep-alive

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 774

   Packet Anomalies 
	Total IPv4 Fragmentation:      125
	Total IPv6 Fragmentation:        0
	Total IPv6 Loop ext headers:     0
	Total TCP bad flags:            24
	Total UDP bogus header:          0
	Total DNS bogus header:          0
	Total DNS long domain name:      0
	Total SMTP bogus header:         0
	Total SMTP long email:           0
	Total IMAP bogus header:         0
   ...

/v1/help
********

Shows the python help available on the module and also is possible to use the parameter "class" to specify a specific class name.

.. code:: bash

   GET /v1/help HTTP/1.1
   Host: 192.168.0.220:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
   Accept-Encoding: gzip, deflate
   Referer: http://192.168.0.220:8080/
   DNT: 1
   Connection: keep-alive
   Upgrade-Insecure-Requests: 1
   Sec-GPC: 1
   Cache-Control: max-age=0

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Type: text/plain
   Content-Length: 134271

   Help on module pyaiengine:

   NAME
       pyaiengine

   CLASSES
       Boost.Python.instance(builtins.object)
           BitcoinInfo
           Cache
           CoAPInfo
           DCERCPInfo
           DHCPInfo
           DHCPv6Info
   ...

/v1/evidences.<pid>.pcap
************************

This url will return the generated pcap file if the evidences feature is enabled.

.. code:: bash

   GET /v1/evidences.8972.pcap HTTP/1.1
   Host: localhost:8081
   User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:99.0) Gecko/20100101 Firefox/99.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
   Accept-Language: en-US,en;q=0.5
   Accept-Encoding: gzip, deflate, br
   Connection: keep-alive
   Referer: http://localhost:8081/

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Content-Length: 16660
   Content-Type: application/vnd.tcpdump.pcap

   ................................a...a.......E..]..@.@.C.
   .aF
   ...

/v1/current_code
****************

This url will return the current code script file that has been used for run the instance.

.. code:: bash

   GET /v1/current_code HTTP/1.1
   Host: 127.0.0.1:46881
   User-Agent: python-requests/2.24.0
   Accept-Encoding: gzip, deflate
   Accept: */*
   Connection: keep-alive

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 2.1.0
   Cache-Control: public
   Content-Type: text/plain
   Content-Length: 669

   # code generated 2022-06-1 18:38:08.965434
   # for test cases on StackVirtualHTTPServerTests
   import sys
   import pyaiengine

   stack = pyaiengine.StackVirtual()

   stack.tcp_flows = 2048
   stack.udp_flows = 1024
   ...

/v1/alarms
**********

This uri will process json data on the payload, this will be used when we
want to send data to the engine, Bear in mind that the json will be process by
the callback set on the property alarm_callback of the PacketDispatcher.

.. code:: bash

   POST /v1/alarms HTTP/1.1
   Host: 127.0.0.1
   User-Agent: Boost.Beast/292
   Content-Type: application/json
   Content-Length: 462

   {"dns":{"domain":"s2.youtube.com","ips":["video-stats.l.google","74.125.24.139","74.125.24.138","74.125.24.100","74.125.24.101","74.125.24.102","74.125.24.113"],"matchs":"Domain1","qtype":1},"downstream":{"bytes":0,"packets":0,"ttl":0},"evidence":false,"ip":{"dst":"192.168.1.1","src":"192.168.1.254"},"label":"F1Q1P357M6T8BS9H8OVD2MOIDMM1NTMC","layer7":"DNS","port":{"dst":63139,"src":53},"proto":17,"reject":false,"upstream":{"bytes":299,"packets":1,"ttl":64}}

The alarm callback will process the json data received and can make actions depending
the use case.

.. code:: bash


   def alarm_callback(data):

       jdata = json.loads(data)

       ...

