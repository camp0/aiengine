/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "ElapsedTime.h"

namespace aiengine {


std::string ElapsedTime::compute(const boost::posix_time::ptime &base_time, const boost::posix_time::ptime &ref_time) const {

	std::ostringstream str_duration;;
        boost::posix_time::time_duration duration(base_time - ref_time);
        int days = duration.hours() / 24;
        int hours = duration.hours();

        if (days > 0)
        	hours = hours - (24 * days);

        str_duration << std::setw(2) << std::setfill('0') << days
        	<< ":" << std::setw(2) << std::setfill('0') << hours
                << ":" << std::setw(2) << std::setfill('0') << duration.minutes()
                << ":" << std::setw(2) << std::setfill('0') << duration.seconds();
        return str_duration.str();
}

std::string ElapsedTime::compute(const time_t ref_time) const {

	boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
        boost::posix_time::ptime reftime = boost::posix_time::from_time_t(ref_time);

        return compute(reftime, epoch);
}

std::string ElapsedTime::compute(const boost::posix_time::ptime &ref_time) const {

	boost::posix_time::ptime current_time = boost::posix_time::microsec_clock::local_time();

        return compute(current_time, ref_time);
}

} // namespace aiengine
