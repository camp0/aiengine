/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * Thio library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 * Configuration diagram of the stack
 *
 *                         +--------------------+
 *                         | TCPGenericProtocol |
 *                         +-------+------------+
 *                                 |
 *          +--------------------+ |              +--------------------+
 *          |     SSLProtocol    | |              | UDPGenericProtocol |
 *          +--------------+-----+ |              +-----------+--------+
 *                         |       |                          |
 * +--------------------+  |       |  +--------------------+  |
 * |    HTTPProtocol    |  |       |  |    DNSProtocol     |  |
 * +------------------+-+  |       |  +------------+-------+  |
 *                    |    |       |               |          |
 *                 +--+----+-------+----+    +-----+----------+---+
 *                 |    TCPProtocol     |    |    UDPProtocol     |
 *                 +------------------+-+    +-+------------------+
 *                                    |        |
 *      +--------------------+        |        |
 *      |   ICMPProtocol     +-----+  |        |
 *      +--------------------+     |  |        |
 *                               +-+--+--------+------+
 *                         +---> |     IPProtocol     | <---+
 *                         |     +---------+----------+     |
 *                         |               |                |
 *                +--------+-----------+   |   +------------+-------+
 *                |    VLANProtocol    |   |   |    MPLSProtocol    |
 *                +--------+-----------+   |   +------------+-------+
 *                         |               |                |
 *                         |     +---------+----------+     |
 *                         +-----+  EthernetProtocol  +-----+
 *                               +--------------------+
 *
 */
#ifndef SRC_STACKLAN_H_
#define SRC_STACKLAN_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <chrono>
#include <string>
#include "Multiplexer.h"
#include "FlowForwarder.h"
#include "protocols/ip/IPProtocol.h"
#include "protocols/icmp/ICMPProtocol.h"
#include "flow/FlowManager.h"
#include "flow/FlowCache.h"
#include "NetworkStack.h"
#include "DatabaseAdaptor.h"
#include "RejectManager.h"

namespace aiengine {

class StackLan: public NetworkStack {
public:
	explicit StackLan();
        virtual ~StackLan() {}

	uint16_t getLinkLayerType() const override { return ETHERTYPE_IP; }
        MultiplexerPtrWeak getLinkLayerMultiplexer() override { return mux_eth;}

        void statistics(std::basic_ostream<char> &out) const override;
	
	void showFlows(std::basic_ostream<char> &out, std::function<bool (const Flow&)> condition, int protocol) const override;
	void showFlows(Json &out, std::function<bool (const Flow&)> condition, int protocol) const override;

	void setTotalTCPFlows(int value) override;
	void setTotalUDPFlows(int value) override;
	int getTotalTCPFlows() const override;
	int getTotalUDPFlows() const override;

	void setMode(const std::string &mode) override;
	const char *getMode() const override { return operation_mode_.c_str(); }

	void setFlowsTimeout(int timeout) override;
	int getFlowsTimeout() const override { return flow_table_tcp_->getTimeout(); }

#if defined(BINDING)
        FlowManager &getTCPFlowManager() override { return *flow_table_tcp_.get();}
        FlowManager &getUDPFlowManager() override { return *flow_table_udp_.get();}
#else
        FlowManagerPtrWeak getTCPFlowManager() override { return flow_table_tcp_; }
        FlowManagerPtrWeak getUDPFlowManager() override { return flow_table_udp_; }
#endif

        void setTCPRegexManager(const SharedPointer<RegexManager> &rm) override;
        void setUDPRegexManager(const SharedPointer<RegexManager> &rm) override;

        void setTCPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) override;
        void setUDPIPSetManager(const SharedPointer<IPSetManager> &ipset_mng) override;

#if defined(RUBY_BINDING) || defined(LUA_BINDING) || defined(GO_BINDING) 
        void setTCPRegexManager(RegexManager &rm) override { setTCPRegexManager(std::make_shared<RegexManager>(rm)); }
        void setUDPRegexManager(RegexManager &rm) override { setUDPRegexManager(std::make_shared<RegexManager>(rm)); }

        void setTCPIPSetManager(IPSetManager &ipset_mng) { setTCPIPSetManager(std::make_shared<IPSetManager>(ipset_mng)); }
        void setUDPIPSetManager(IPSetManager &ipset_mng) { setUDPIPSetManager(std::make_shared<IPSetManager>(ipset_mng)); }

#elif defined(JAVA_BINDING)
        void setTCPRegexManager(RegexManager *sig);
        void setUDPRegexManager(RegexManager *sig);
        
	void setTCPIPSetManager(IPSetManager *ipset_mng); 
        void setUDPIPSetManager(IPSetManager *ipset_mng);
#endif

	void setAsioService(boost::asio::io_service &io_service, const std::string &device) override;
	void stopAsioService() override;

	std::tuple<Flow*,Flow*> getCurrentFlows() const override; 
	SharedPointer<Flow> getFlow(const FlowSearchOptions &fsos) const override;
private:
	typedef NetworkStack super_;

	std::string operation_mode_ = "full";

        //Protocols
	IPProtocolPtr ip_ = IPProtocolPtr(new IPProtocol());
        UDPProtocolPtr udp_ = UDPProtocolPtr(new UDPProtocol());
        TCPProtocolPtr tcp_ = TCPProtocolPtr(new TCPProtocol());
        ICMPProtocolPtr icmp_ = ICMPProtocolPtr(new ICMPProtocol());

        // Multiplexers
        MultiplexerPtr mux_udp_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_tcp_ = MultiplexerPtr(new Multiplexer());
        MultiplexerPtr mux_icmp_ = MultiplexerPtr(new Multiplexer());

        // FlowManager and FlowCache
        FlowManagerPtr flow_table_udp_ = FlowManagerPtr(new FlowManager());
        FlowManagerPtr flow_table_tcp_ = FlowManagerPtr(new FlowManager());
        FlowCachePtr flow_cache_udp_ = FlowCachePtr(new FlowCache());
        FlowCachePtr flow_cache_tcp_ = FlowCachePtr(new FlowCache());

        // FlowForwarders
        SharedPointer<FlowForwarder> ff_tcp_ = SharedPointer<FlowForwarder>(new FlowForwarder());
        SharedPointer<FlowForwarder> ff_udp_ = SharedPointer<FlowForwarder>(new FlowForwarder());
#if defined(HAVE_REJECT_FLOW)
	SharedPointer<RejectManager<StackLan>> rj_mng_ = SharedPointer<RejectManager<StackLan>>(new RejectManager<StackLan>());
#endif
};

typedef std::shared_ptr<StackLan> StackLanPtr;

} // namespace aiengine

#endif  // SRC_STACKLAN_H_
