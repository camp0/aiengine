/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#ifndef SRC_STRINGMAP_H_
#define SRC_STRINGMAP_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <map>
#include <boost/utility/string_ref.hpp>
#include "Pointer.h"
#include "StringCache.h"
#include "Json.h"

namespace aiengine {

struct StringCacheHits {
public:
	StringCacheHits(const SharedPointer<StringCache> &s):
		sc(s),
		hits(1) {}

	SharedPointer<StringCache> sc;
	uint32_t hits;
};

typedef std::map<boost::string_ref, StringCacheHits> GenericMapType;
typedef std::pair<boost::string_ref, StringCacheHits> PairStringCacheHits;

class StringMap : public GenericMapType {
public:
	explicit StringMap(const std::string &name, const std::string &row_name):
		name_(name),
		row_name_(row_name)
	{}
    	virtual ~StringMap() {}

	void show(std::basic_ostream<char> &out, const char *tab, int32_t limit) const;
	void show(Json &out, int32_t limit) const;
private:
	void sort(std::vector<PairStringCacheHits> &g_list) const;

	std::string name_ = "";
	std::string row_name_ = "";
};

} // namespace aiengine

#endif  // SRC_STRINGMAP_H_
