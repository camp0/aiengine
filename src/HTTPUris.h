/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */

#ifndef SRC_HTTPURIS_H_
#define SRC_HTTPURIS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

namespace aiengine {

#define BASE_URI "/v1"

class HTTPUris {
public:
	static constexpr std::string_view show_protocols_summary = BASE_URI "/protocols/summary";
	static constexpr std::string_view show_protocol = BASE_URI "/protocol";
	static constexpr std::string_view show_network_flows = BASE_URI "/flows";
	static constexpr std::string_view show_summary = BASE_URI "/summary";
	static constexpr std::string_view show_system = BASE_URI "/system";
	static constexpr std::string_view show_uris = BASE_URI "/uris";
	static constexpr std::string_view pcapfile = BASE_URI "/pcapfile";
	static constexpr std::string_view python_script = BASE_URI "/python_code";
	static constexpr std::string_view show_network_flow = BASE_URI "/flow";
	static constexpr std::string_view show_python_locals = BASE_URI "/locals";
	static constexpr std::string_view show_python_help = BASE_URI "/help";
	static constexpr std::string_view show_python_code = BASE_URI "/current_code";
	static constexpr std::string_view show_current_packet = BASE_URI "/current_packet";
	static constexpr std::string_view show_logs = BASE_URI "/logs";
	static constexpr std::string_view show_health = BASE_URI "/health";
	static constexpr std::string_view show_anomalies = BASE_URI "/anomalies";
	static constexpr std::string_view show_evidences = BASE_URI "/evidences";
	static constexpr std::string_view alarms = BASE_URI "/alarms";
};

} // namespace aiengine

#endif  // SRC_HTTPURIS_H_
