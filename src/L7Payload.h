/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_L7PAYLOAD_H_
#define SRC_L7PAYLOAD_H_

#include <iostream>
#include <memory>

namespace aiengine {

static uint16_t total_l7_allocations = 0;

/* 
 * The next defines control the amount of memory that will be
 * allocated for store the payloads of the packets at layer 7
 *
 * If you want to change the is up to you depending on how
 * much memory you want to consume.
 *
 * The maximum memory by default will be 131Kbytes
 */
#define MAX_L7PAYLOAD 512
#define MAX_L7_ALLOCATIONS 256

class L7Payload {
public:
	explicit L7Payload() { ++total_l7_allocations; }

	virtual ~L7Payload() { --total_l7_allocations; }

	void copy(const uint8_t *payload, uint16_t length) {

		length_ = length;

		if (length > MAX_L7PAYLOAD)
			length_ = MAX_L7PAYLOAD;
	
		std::memcpy(buffer_, payload, length_);
	}

	uint16_t length() const { return length_; }

	const uint8_t *payload() const { return buffer_; }

private:
	uint8_t buffer_[MAX_L7PAYLOAD];
	uint16_t length_ = 0;
};

typedef std::shared_ptr<L7Payload> L7PayloadPtr;

} // namespace aiengine

#endif  // SRC_L7PAYLOAD_H_
