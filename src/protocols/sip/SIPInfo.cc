/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SIPInfo.h"
#include "SIPProtocol.h"

namespace aiengine {

void SIPInfo::reset() { 

	resetStrings();
	state_ = SIP_NONE;
	src_port = 0;
	dst_port = 0;
	src_addr.s_addr = 0;
	dst_addr.s_addr = 0;
}

void SIPInfo::resetStrings() { 

	uri.reset(); 
	from.reset(); 
	to.reset(); 
	via.reset(); 
}

std::ostream& operator<< (std::ostream &out, const SIPInfo &info) {

	if (info.uri) 
		out << " Uri:" << info.uri->name();

	if (info.from) 
		out << " From:" << info.from->name();

	if (info.to) 
		out << " To:" << info.to->name();

        if (info.via) 
		out << " Via:" << info.via->name();

	if (info.state_ == SIP_CALL_ESTABLISHED) 
		out << " OnCall";

	if (info.state_ == SIP_TRYING_CALL) 
		out << " TryingCall";
 
        return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SIPInfo &info) {

	if (info.uri) 
		out["uri"] = info.uri->name();

	if (info.from) 
		out["from"] = info.from->name();

	if (info.to) 
		out["to"] = info.to->name();

        if (info.via) 
		out["via"] = info.via->name();

	if (info.state_ == SIP_CALL_ESTABLISHED) {
		out["status"] = "OnCall";
		out["voip"]["ip"]["src"] = inet_ntoa(info.src_addr);
		out["voip"]["ip"]["dst"] = inet_ntoa(info.dst_addr);
		out["voip"]["port"]["src"] = info.src_port;
		out["voip"]["port"]["dst"] = info.dst_port;

	} else if (info.state_ == SIP_TRYING_CALL) 
		out["status"] = "TryingCall";

	return out;
}

} // namespace aiengine
