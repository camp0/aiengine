/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_UDPGENERIC_UDPGENERICPROTOCOL_H_
#define SRC_PROTOCOLS_UDPGENERIC_UDPGENERICPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include "FlowRegexEvaluator.h"
#include "regex/RegexManager.h"
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>

namespace aiengine {

class UDPGenericProtocol: public Protocol {
public:
	explicit UDPGenericProtocol():
		Protocol("UDPGeneric", IPPROTO_UDP) {}
    	virtual ~UDPGenericProtocol() {}

	static const uint16_t header_size = 0;

	uint16_t getId() const override { return 0x0000; }
	uint16_t getHeaderSize() const override { return header_size;}

	// Condition for say that a payload is for generic udp 
	// Accepts all!
	bool check(const Packet &packet) override; 
	bool processPacket(Packet &packet) override { return true; }
	void processFlow(Flow *flow) override;

	void statistics(std::basic_ostream<char> &out, int level, int32_t limit) const override;
	void statistics(Json &out, int level) const override;

        void releaseCache() override {} // No need to free cache

        void setHeader(const uint8_t *raw_packet) override {
        
                header_ = raw_packet;
        }

	void setRegexManager(const SharedPointer<RegexManager> &rm); 

	uint64_t getCurrentUseMemory() const override { return sizeof(UDPGenericProtocol); }
	uint64_t getAllocatedMemory() const override { return sizeof(UDPGenericProtocol); }
	uint64_t getTotalAllocatedMemory() const override { return sizeof(UDPGenericProtocol); }

        void setDynamicAllocatedMemory(bool value) override {}
        bool isDynamicAllocatedMemory() const override { return false; }

	uint32_t getTotalEvents() const override { return eval_.getTotalMatches(); }

	CounterMap getCounters() const override; 
	void resetCounters() override;

private:
	const uint8_t *header_ = nullptr;
	SharedPointer<RegexManager> rm_ = nullptr;
	FlowRegexEvaluator eval_ {};
};

typedef std::shared_ptr<UDPGenericProtocol> UDPGenericProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_UDPGENERIC_UDPGENERICPROTOCOL_H_
