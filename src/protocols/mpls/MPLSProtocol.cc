/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "MPLSProtocol.h"
#include <iomanip> // setw
#include <bitset>

namespace aiengine {

MPLSProtocol::MPLSProtocol():
	Protocol("MPLS") {}

bool MPLSProtocol::check(const Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		setHeader(packet.getPayload());
		++total_valid_packets_;
		return true;
	} else {
		++total_invalid_packets_;
		return false;
	}
}

bool MPLSProtocol::processPacket(Packet &packet) {

	CPUCycle cycles(&total_cpu_cycles_);
        MultiplexerPtr mux = mux_.lock();
        ++total_packets_;
        total_bytes_ += packet.getLength();

        if (mux) {
		uint32_t label;
		int mpls_header_size = 0;
		int counter = 0;
		const uint8_t *mpls_header = header_;
		bool sw = true;

		// Process the MPLS Header and forward to the next level
		do {
			label = mpls_header[0] << 12;
			label |= mpls_header[1] << 4;
			label |= mpls_header[2] >> 4;
	
			std::bitset<1> b1(mpls_header[2]);

			mpls_header = (mpls_header + 4);
			mpls_header_size += 4;
			++counter;
			if ((b1[0] == true)||(counter > 2)) sw = false;
		} while(sw);

		mux->setHeaderSize(mpls_header_size);			       
		packet.setPrevHeaderSize(mpls_header_size); 
		mux->setNextProtocolIdentifier(ETHERTYPE_IP);
        }

	return true;
}

void MPLSProtocol::statistics(std::basic_ostream<char> &out, int level, int32_t limit) const {

	showStatisticsHeader(out, level);

	if ((level > 5)and(mux_.lock()))
		mux_.lock()->statistics(out);
}

void MPLSProtocol::statistics(Json &out, int level) const {          

	showStatisticsHeader(out, level);
}

CounterMap MPLSProtocol::getCounters() const {
	CounterMap cm;
 
        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void MPLSProtocol::resetCounters() {

	reset();
}

} // namespace aiengine
