/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "MQTTInfo.h"

namespace aiengine {

void MQTTInfo::reset() { 

	have_data_ = false;
	command_ = 0;
	total_server_commands_ = 0;
	total_client_commands_ = 0;
	data_chunk_length_ = 0;
	topic.reset();
}

std::ostream& operator<< (std::ostream &out, const MQTTInfo &info) {

	out << " Cmd(" << (int)info.getCommand();
	out << ")Cli(" << info.getTotalClientCommands();
	out << ")Ser(" << info.getTotalServerCommands() << ") ";

	if (info.topic) out << " Topic:" << info.topic->name();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const MQTTInfo &info) {

	out["operation"] = info.getCommand();
	out["total_client"] = info.getTotalClientCommands();
	out["total_server"] = info.getTotalServerCommands();

	if (info.topic)
		out["topic"] = info.topic->name();

	return out;
}
	
} // namespace aiengine
