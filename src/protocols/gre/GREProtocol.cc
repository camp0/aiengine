/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "GREProtocol.h"
#include <iomanip>

namespace aiengine {

GREProtocol::GREProtocol():
	Protocol("GRE") {}

bool GREProtocol::check(const Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		setHeader(packet.getPayload());

		if ((getProtocol() == ETH_P_TEB)or(getProtocol() == ETH_P_IP)) {
			++total_valid_packets_;
			return true;
		}
	}
	++total_invalid_packets_;
	return false;
}

bool GREProtocol::processPacket(Packet &packet) {

	CPUCycle cycles(&total_cpu_cycles_);
        ++total_packets_;
        total_bytes_ += packet.getLength();

	if (!mux_.expired()) {
        	MultiplexerPtr mux = mux_.lock();
		uint16_t proto = ETH_P_IP;
		int offset = 0;

		if (getChecksumBit()) offset = 4;
		if (getKeyBit()) offset += 4;
		if (getSequenceNumberBit()) offset += 4;
		if (getRoutingBit()) offset += 4;
		if (getProtocol() == ETH_P_TEB) proto = 0;

		mux->setNextProtocolIdentifier(proto);
                mux->setHeaderSize(header_size + offset);
		packet.setPrevHeaderSize(header_size + offset);
        }
	return true;
}

void GREProtocol::statistics(std::basic_ostream<char> &out, int level, int32_t limit) const {

	showStatisticsHeader(out, level);

	if ((level > 5)and(mux_.lock()))
		mux_.lock()->statistics(out);
}

void GREProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);
}

CounterMap GREProtocol::getCounters() const {
 	CounterMap cm; 

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);

        return cm;
}

void GREProtocol::resetCounters() {

	reset();

}

} // namespace aiengine
