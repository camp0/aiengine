/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "SSLInfo.h"

namespace aiengine {

void SSLInfo::reset() {

	host_name.reset();
	issuer.reset();
	session_id.reset();
#if defined(HAVE_JA3)
	ja3_fingerprint.reset();
#endif
	matched_domain_name.reset();
	is_banned_ = false;
	encrypted_ = false;
	data_pdus_ = 0;
	version_ = 0;
	heartbeat_ = false;
	alert_ = false;
	alert_code_ = 0;
	cipher_ = 0xFFFF;
	mtls_ = false;
}

std::ostream& operator<< (std::ostream &out, const SSLInfo &info) {

	out << " Pdus:" << info.getTotalDataPdus();

	if (info.version_ > 0)
		out << " Ver:0x" << std::hex << info.version_ << std::dec;

	if (info.cipher_ != 0xFFFF)
		out << " Cipher:0x" << std::hex << info.cipher_ << std::dec;

	if (info.mtls_)
		out << " MTLS";

	if (info.is_banned_)
		out << " Banned";

	if (info.host_name)
		out << " Host:" << info.host_name->name();

	if (info.issuer)
		out << " Issuer:" << info.issuer->name();

	if (info.session_id)
		out << " Session:" << info.session_id->name();
#if defined(HAVE_JA3)
	if (info.ja3_fingerprint)
		out << " Fingerprint:" << info.ja3_fingerprint->name();
#endif
	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SSLInfo &info) {

        out["pdus"] = info.getTotalDataPdus();
        out["cipher"] = info.cipher_;
        out["version"] = info.version_;

	if (info.mtls_)
		out["mtls"] = true;

	if (info.alert_)
		out["alert"] = info.alert_code_;

	if (info.heartbeat_)
		out["heartbeat"] = true;

        if (info.is_banned_)
		out["banned"] = true;

        if (info.host_name)
		out["host"] = info.host_name->name();

        if (info.issuer)
		out["issuer"] = info.issuer->name();

        if (info.session_id)
		out["session"] = info.session_id->name();

	if (info.matched_domain_name)
		out["matchs"] = info.matched_domain_name->name();
#if defined(HAVE_JA3)
        if (info.ja3_fingerprint)
		out["fingerprint"] = info.ja3_fingerprint->name();
#endif
        return out;
}

} // namespace aiengine
