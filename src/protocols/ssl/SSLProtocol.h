/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#ifndef SRC_PROTOCOLS_SSL_SSLPROTOCOL_H_
#define SRC_PROTOCOLS_SSL_SSLPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include <boost/utility/string_ref.hpp>

#ifdef HAVE_JA3
#include <openssl/md5.h>
#endif

#include "Protocol.h"
#include "SSLInfo.h"
#include "Cache.h"
#include "flow/FlowManager.h"

namespace aiengine {

// Minium SSL header
struct ssl_record {
	uint8_t 	type; 		/* SSL record type */
	uint16_t 	version; 	/* SSL version (major/minor) */
	uint16_t 	length; 	/* Length of data in the record (excluding the header itself), The maximum SSL supports is 16384 (16K). */
	uint8_t 	data[0];
} __attribute__((packed));

struct handshake_record {
	uint8_t		type;
	uint8_t		padd;
	uint16_t	length;
	uint8_t		data[0];
} __attribute__((packed));

// The only supported versions
#define SSL3_VERSION 0x0300
#define TLS1_VERSION 0x0301
#define TLS1_1_VERSION 0x0302
#define TLS1_2_VERSION 0x0303
#define TLS1_3_VERSION 0x0304 // Beta mode

// Record_type
#define SSL3_CT_HANDSHAKE		22
#define SSL3_CT_ALERT 			21
#define SSL3_CT_CHANGE_CIPHER_SPEC 	20
#define SSL3_CT_APPLICATION_DATA 	23

// Record types of the ssl_handshake_record
#define SSL3_MT_HELLO_REQUEST            0   //(x'00')
#define SSL3_MT_CLIENT_HELLO             1   //(x'01')
#define SSL3_MT_SERVER_HELLO             2   //(x'02')
#define SSL3_MT_NEW_SESSION_TICKET       4   //(x'04')
#define SSL3_MT_CERTIFICATE             11   //(x'0B')
#define SSL3_MT_SERVER_KEY_EXCHANGE     12   // (x'0C')
#define SSL3_MT_CERTIFICATE_REQUEST     13   // (x'0D')
#define SSL3_MT_SERVER_DONE             14   // (x'0E')
#define SSL3_MT_CERTIFICATE_VERIFY      15   // (x'0F')
#define SSL3_MT_CLIENT_KEY_EXCHANGE     16   // (x'10')
#define SSL3_MT_FINISHED                20   // (x'14')

struct ssl_hello {
	uint8_t 	handshake_type[2];
	uint16_t 	length;
	uint16_t 	version;
	uint8_t 	random[32];
	uint8_t 	session_id_length;
	uint8_t 	data[0];
} __attribute__((packed));

struct ssl_extension {
	uint16_t 	type;
	uint16_t	length;
	uint8_t 	data[0];
} __attribute__((packed));

struct ssl_server_name {
	uint16_t 	list_length;
	uint8_t 	type;
	uint16_t 	length;
	uint8_t 	data[0];
} __attribute__((packed));

struct ssl_cert {
	uint16_t 	type;
	uint16_t 	length;
	uint8_t 	pad1;
	uint16_t 	cert_length;
	uint8_t 	data[0];
} __attribute__((packed));

class SSLProtocol: public Protocol {
public:
	explicit SSLProtocol():
		Protocol("SSL", IPPROTO_TCP) {}
    	virtual ~SSLProtocol();

	std::string_view esni_label = "esni";
	static const uint16_t header_size = sizeof(ssl_record);

	uint16_t getId() const override { return 0x0000; }
	uint16_t getHeaderSize() const override { return header_size; }

	// Condition for say that a payload is ssl
	bool check(const Packet &packet) override;
	bool processPacket(Packet &packet) override { return true; }
	void processFlow(Flow *flow) override;

	void statistics(std::basic_ostream<char> &out, int level, int32_t limit) const override;
	void statistics(Json &out, int level) const override;
	void statistics(Json &out, const std::string &map_name, int32_t limit) const override;

	void releaseCache() override;

        void setHeader(const uint8_t *raw_packet) override {

                header_ = reinterpret_cast<const ssl_record*>(raw_packet);
        }

        void increaseAllocatedMemory(int value) override;
        void decreaseAllocatedMemory(int value) override;

	void setDomainNameManager(const SharedPointer<DomainNameManager> &dm) override;
	void setDomainNameBanManager(const SharedPointer<DomainNameManager> &dm) override { ban_domain_mng_ = dm; }

	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	uint64_t getCurrentUseMemory() const override;
	uint64_t getAllocatedMemory() const override;
	uint64_t getTotalAllocatedMemory() const override;

        void setDynamicAllocatedMemory(bool value) override;
        bool isDynamicAllocatedMemory() const override;

	uint32_t getTotalCacheMisses() const override;
	uint32_t getTotalEvents() const override { return total_events_; }

	CounterMap getCounters() const override;
	void resetCounters() override;

#if defined(PYTHON_BINDING)
	boost::python::dict getCacheData(const std::string &name) const override;
	SharedPointer<Cache<StringCache>> getCache(const std::string &name) override;
#elif defined(RUBY_BINDING)
        VALUE getCacheData(const std::string &name) const;
#endif

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	// Just for testing purposes on the unit test
	Cache<StringCache>::CachePtr getHostCache() const { return host_cache_; }
	StringMap *getHostMap() { return &host_map_; }
	StringMap *getIssuerMap() { return &issuer_map_; }
        uint32_t getTotalHandshakes() const { return total_handshakes_; }
        uint32_t getTotalEncryptedHandshakes() const { return total_encrypted_handshakes_; }
        uint32_t getTotalAlerts() const { return total_alerts_; }
        uint32_t getTotalChangeCipherSpecs() const { return total_change_cipher_specs_; }
        uint32_t getTotalDatas() const { return total_data_; }

        uint32_t getTotalClientHellos() const { return total_client_hellos_; }
        uint32_t getTotalServerHellos() const { return total_server_hellos_; }
        uint32_t getTotalCertificates() const { return total_certificates_; }
        uint32_t getTotalCertificateRequests() const { return total_certificate_requests_; }
        uint32_t getTotalCertificateVerifies() const { return total_certificate_verifies_; }
        uint32_t getTotalServerDones() const { return total_server_dones_; }
	uint32_t getTotalServerKeyExchanges() const {  return total_server_key_exchanges_; }
	uint32_t getTotalClientKeyExchanges() const {  return total_client_key_exchanges_; }
	uint32_t getTotalHandshakeFinishes() const { return total_handshake_finishes_; }
	uint32_t getTotalNewSessionTickets() const { return total_new_session_tickets_; }
        uint32_t getTotalRecords() const { return total_records_; }
        uint32_t getTotalBanHosts() const { return total_ban_hosts_; }
        uint32_t getTotalAllowHosts() const { return total_allow_hosts_; }
#endif

	void setAnomalyManager(SharedPointer<AnomalyManager> amng) override { anomaly_ = amng; }

	Flow* getCurrentFlow() const { return current_flow_; }

	void releaseFlowInfo(Flow *flow) override;

private:
	uint64_t compute_memory_used_by_maps() const;
	void handle_handshake(SSLInfo *info, const ssl_record *record, int length);
	void handle_client_hello(SSLInfo *info, const uint8_t *data, int length);
	void handle_server_hello(SSLInfo *info, const uint8_t *data, int length);
	void handle_certificate(SSLInfo *info, const uint8_t *data, int length);
	void handle_issuer_certificate(SSLInfo *info, const uint8_t *data, int length);
	void attach_host(SSLInfo *info, const boost::string_ref &servername);
	void attach_common_name(SSLInfo *info, const boost::string_ref &name);
	void attach_session(SSLInfo *info, const boost::string_ref &session);
#if defined(HAVE_JA3)
	void attach_ja3_fingerprint(SSLInfo *info, const boost::string_ref &fingerprint);
#endif
	uint8_t get_asn1_length(uint8_t byte);

	const ssl_record *header_ = nullptr;
        uint32_t total_events_ = 0;
	// content types
	uint32_t total_handshakes_ = 0;
	uint32_t total_encrypted_handshakes_ = 0;
	uint32_t total_alerts_ = 0;
	uint32_t total_change_cipher_specs_ = 0;
	uint32_t total_data_ = 0;
	// handshake types
	uint32_t total_client_hellos_ = 0;
	uint32_t total_server_hellos_ = 0;
	uint32_t total_certificates_ = 0;
	uint32_t total_server_key_exchanges_ = 0;
	uint32_t total_certificate_requests_ = 0;
	uint32_t total_server_dones_ = 0;
	uint32_t total_certificate_verifies_ = 0;
	uint32_t total_client_key_exchanges_ = 0;
	uint32_t total_handshake_finishes_ = 0;
	uint32_t total_new_session_tickets_ = 0;
	uint32_t total_records_ = 0;
	uint32_t total_ban_hosts_ = 0;
	uint32_t total_allow_hosts_ = 0;

	Cache<SSLInfo>::CachePtr info_cache_ = Cache<SSLInfo>::CachePtr(new Cache<SSLInfo>("SSL Info cache"));
	Cache<StringCache>::CachePtr host_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("Host cache"));
	Cache<StringCache>::CachePtr issuer_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("Issuer cache"));
	Cache<StringCache>::CachePtr session_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("Session cache"));
#if defined(HAVE_JA3)
	Cache<StringCache>::CachePtr ja3_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("JA3 cache"));
        StringMap ja3_map_ {"SSL Fingerprints", "Fingerprint"};
#endif
        StringMap host_map_ {"SSL Hosts", "Host"};
        StringMap issuer_map_ {"SSL Issuers", "Issuer"};
        StringMap session_map_ {"SSL Sessions", "Session"};

        SharedPointer<DomainNameManager> domain_mng_ = nullptr;
        SharedPointer<DomainNameManager> ban_domain_mng_ = nullptr;
	FlowManagerPtrWeak flow_mng_ = FlowManagerPtrWeak();
	Flow *current_flow_ = nullptr; // For accessing for logging
	SharedPointer<AnomalyManager> anomaly_ = nullptr;
};

typedef std::shared_ptr<SSLProtocol> SSLProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_SSL_SSLPROTOCOL_H_
