/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_RTP_RTPPROTOCOL_H_
#define SRC_PROTOCOLS_RTP_RTPPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include <arpa/inet.h>
#include "flow/FlowManager.h"
#include "Cache.h"

namespace aiengine {

#define RTP_VERSION 2 

struct rtp_header {
	uint8_t 	version;   	/* protocol version */
    	uint8_t 	payload_type;   /* payload type */
    	uint16_t 	seq;      	/* sequence number */
    	uint32_t 	ts;            /* timestamp */
	uint32_t 	ssrc;          /* synchronization source */
} __attribute__((packed));

class RTPProtocol: public Protocol {
public:
	explicit RTPProtocol():
		Protocol("RTP", IPPROTO_UDP) {}
    	virtual ~RTPProtocol();

	static constexpr uint16_t header_size = sizeof(rtp_header);

	uint16_t getId() const override { return 0x0000; }
	uint16_t getHeaderSize() const override { return header_size;}

	// Condition for say that a packet is rtp
	bool check(const Packet &packet) override; 
        void processFlow(Flow *flow) override;
        bool processPacket(Packet& packet) override { return true; } 

	void statistics(std::basic_ostream<char>& out, int level, int32_t limit) const override;
	void statistics(Json &out, int level) const override;

	void releaseCache() override {}

	void setHeader(const uint8_t *raw_packet) override { 

		header_ = reinterpret_cast <const rtp_header*> (raw_packet);
	}

	// Protocol specific
	uint8_t getVersion() const { return (header_->version >> 6); }
	bool getPadding() const { return (header_->version & (1 << 5)); } 
	uint8_t getPayloadType() const; 

	uint64_t getCurrentUseMemory() const override { return sizeof(RTPProtocol); }
	uint64_t getAllocatedMemory() const override;
	uint64_t getTotalAllocatedMemory() const override;

        void setDynamicAllocatedMemory(bool value) override {}
        bool isDynamicAllocatedMemory() const override { return false; }

	CounterMap getCounters() const override; 
	void resetCounters() override;

        void setAnomalyManager(SharedPointer<AnomalyManager> amng) override { anomaly_ = amng; }

	Flow *getCurrentFlow() const { return current_flow_; }
private:
	const rtp_header *header_ = nullptr;

	// Some statistics 

        Flow *current_flow_ = nullptr;
        SharedPointer<AnomalyManager> anomaly_ = nullptr;
};

typedef std::shared_ptr<RTPProtocol> RTPProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_RTP_RTPPROTOCOL_H_
