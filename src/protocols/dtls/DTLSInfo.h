/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_DTLS_DTLSINFO_H_
#define SRC_PROTOCOLS_DTLS_DTLSINFO_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "StringCache.h"
#include "names/DomainName.h"
#include "FlowInfo.h"

namespace aiengine {

class DTLSInfo : public FlowInfo {
public:
        explicit DTLSInfo() { reset(); }
        virtual ~DTLSInfo() {}

        void reset(); 

	void setEncrypted(bool value) { encrypted_ = value; }
	bool isEncrypted() const { return encrypted_; }

	void incDataPdus() { ++data_pdus_; }
	int32_t getTotalDataPdus() const { return data_pdus_; }

	void setVersion(uint16_t version) { version_ = version; }
	uint16_t getVersion() const { return version_; }

	void setEpoch(uint16_t value) { epoch_ = value; }
	uint16_t getEpoch() const { return epoch_; }

        friend std::ostream& operator<< (std::ostream &out, const DTLSInfo &info);
	friend nlohmann::json& operator<< (nlohmann::json& out, const DTLSInfo &info);

private:
	bool encrypted_:1;
	uint16_t version_;
	uint16_t epoch_;
	int32_t data_pdus_;
};

} // namespace aiengine  

#endif  // SRC_PROTOCOLS_DTLS_DTLSINFO_H_
