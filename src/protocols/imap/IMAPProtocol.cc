/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "IMAPProtocol.h"
#include <iomanip> // setw

namespace aiengine {

// List of support command from the client
std::vector<ImapCommandType> IMAPProtocol::commands_ {
        std::make_tuple("CAPABILITY"    ,10,    "capabilities"  ,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_CAPABILITY)),
        std::make_tuple("STARTTLS"      ,8,     "starttls"      ,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_STARTTLS)),
        std::make_tuple("AUTHENTICATE"  ,12,    "authenticates" ,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_AUTHENTICATE)),
        std::make_tuple("UID"           ,3,     "uids"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_UID)),
        std::make_tuple("LOGIN"      	,5,     "logins"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_LOGIN)),
        std::make_tuple("SELECT"      	,6,     "selects"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_SELECT)),
        std::make_tuple("EXAMINE"      	,7,     "examines"      ,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_EXAMINE)),
        std::make_tuple("CREATE"      	,6,     "createss"      ,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_CREATE)),
        std::make_tuple("DELETE"      	,6,     "deletes"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_DELETE)),
        std::make_tuple("RENAME"      	,6,     "renames"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_RENAME)),
	std::make_tuple("SUBSCRIBE"    	,9,     "subscribes"   	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_SUBSCRIBE)),
        std::make_tuple("UNSUBSCRIBE"  	,11,    "unsubscribes" 	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_UNSUBSCRIBE)),
        std::make_tuple("LIST"      	,4,     "lists"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_LIST)),
        std::make_tuple("LSUB"      	,4,     "lsub"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_LSUB)),
        std::make_tuple("STATUS"      	,6,     "status"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_STATUS)),
        std::make_tuple("APPEND"      	,6,     "appends"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_APPEND)),
        std::make_tuple("CHECK"      	,5,     "checks"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_CHECK)),
        std::make_tuple("CLOSE"      	,5,     "closes"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_CLOSE)),
	std::make_tuple("EXPUNGE"      	,7,     "expunges"     	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_EXPUNGE)),
	std::make_tuple("SEARCH"      	,6,     "searches"     	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_SEARCH)),
	std::make_tuple("FETCH"      	,5,     "fetchs"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_FETCH)),
	std::make_tuple("STORE"      	,5,     "stores"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_STORE)),
	std::make_tuple("COPY"      	,4,     "copies"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_COPY)),
	std::make_tuple("NOOP"      	,4,     "noops"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_NOOP)),
	std::make_tuple("LOGOUT"      	,6,     "logouts"      	,0,     static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_LOGOUT))
};

IMAPProtocol::IMAPProtocol():
	Protocol("IMAP", IPPROTO_TCP) {}

IMAPProtocol::~IMAPProtocol() {

	anomaly_.reset();
}

bool IMAPProtocol::check(const Packet &packet) {

	const uint8_t *payload = packet.getPayload();

	if ((payload[0] == '*')and(payload[1] == ' ')and(payload[2] == 'O')and
		(payload[3] == 'K')and(payload[4] == ' ')and
		(packet.getSourcePort() == 143)) {

		++total_valid_packets_;
		return true;
	} else {
		++total_invalid_packets_;
		return false;
	}
}

void IMAPProtocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);
	user_cache_->setDynamicAllocatedMemory(value);
}

bool IMAPProtocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();
}

uint64_t IMAPProtocol::getCurrentUseMemory() const {

        uint64_t mem = sizeof(IMAPProtocol);

        mem += info_cache_->getCurrentUseMemory();
        mem += user_cache_->getCurrentUseMemory();

        return mem;
}

uint64_t IMAPProtocol::getAllocatedMemory() const {

        uint64_t mem = sizeof(IMAPProtocol);

        mem += info_cache_->getAllocatedMemory();
        mem += user_cache_->getAllocatedMemory();

        return mem;
}

uint64_t IMAPProtocol::getTotalAllocatedMemory() const {

        uint64_t mem = getAllocatedMemory();

	mem += compute_memory_used_by_maps();

	return mem;
}

uint64_t IMAPProtocol::compute_memory_used_by_maps() const {

	uint64_t bytes = 0;

	std::for_each (user_map_.begin(), user_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});
	return bytes;
}

uint32_t IMAPProtocol::getTotalCacheMisses() const {

	uint32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += user_cache_->getTotalFails();

	return miss;
}

void IMAPProtocol::setDomainNameManager(const SharedPointer<DomainNameManager> &dm) {

	if (domain_mng_)
		domain_mng_->setPluggedToName("");

	if (dm) {
		domain_mng_ = dm;
		domain_mng_->setPluggedToName(name());
	} else {
		domain_mng_.reset();
	}
}

void IMAPProtocol::releaseCache() {

	if (FlowManagerPtr fm = flow_mng_.lock(); fm) {
		auto ft = fm->getFlowTable();

		std::ostringstream msg;
        	msg << "Releasing " << name() << " cache";

		infoMessage(msg.str());

		uint64_t total_cache_bytes_released = compute_memory_used_by_maps();
		uint64_t total_bytes_released_by_flows = 0;
		uint64_t total_cache_save_bytes = 0;
		uint32_t release_flows = 0;
                uint32_t release_user = user_map_.size();

                for (auto &flow: ft) {
                       	if (SharedPointer<IMAPInfo> info = flow->getIMAPInfo(); info) {
                                total_bytes_released_by_flows += sizeof(info);

                                flow->layer7info.reset();
                                ++release_flows;
                                info_cache_->release(info);
                        }
                }
                // Some entries can be still on the maps and needs to be
                // retrieve to their existing caches
                for (auto &entry: user_map_) {
			total_cache_save_bytes += entry.second.sc->size() * (entry.second.hits - 1);
                        releaseStringToCache(user_cache_, entry.second.sc);
		}
		user_map_.clear();

		data_time_ = boost::posix_time::microsec_clock::local_time();

                msg.str("");
                msg << "Release " << release_user << " user names, " << release_flows << " flows";
		computeMemoryUtilization(msg, total_cache_bytes_released, total_bytes_released_by_flows, total_cache_save_bytes);
                infoMessage(msg.str());
	}
}

void IMAPProtocol::releaseFlowInfo(Flow *flow) {

	if (auto info = flow->getIMAPInfo(); info)
                info_cache_->release(info);
}

void IMAPProtocol::attach_user_name(IMAPInfo *info, const boost::string_ref &name) {

	if (!info->user_name) {
                if (StringMap::iterator it = user_map_.find(name); it != user_map_.end()) {
                        ++(it->second).hits;
                        info->user_name = (it->second).sc;
		} else {
                        if (SharedPointer<StringCache> user_ptr = user_cache_->acquire(); user_ptr) {
                                user_ptr->name(name.data(),name.length());
                                info->user_name = user_ptr;
                                user_map_.insert(std::make_pair(user_ptr->name(), user_ptr));
                        }
                }
        }
}

void IMAPProtocol::handle_cmd_login(IMAPInfo *info, const boost::string_ref &header) {

	boost::string_ref domain;
	boost::string_ref user_name;

        size_t token = header.find("@");
   	size_t end = header.find(" ");

	if (end < header.length()) {
		domain = header.substr(0, end);
		user_name = header.substr(0, end);
	} else {
		++total_events_;
	       	if (current_flow_->getPacketAnomaly() == PacketAnomalyType::NONE)
                        current_flow_->setPacketAnomaly(PacketAnomalyType::IMAP_BOGUS_HEADER);

		anomaly_->incAnomaly(PacketAnomalyType::IMAP_BOGUS_HEADER);
		return;
	}

	if (token < header.length()) {
		// The name have the domain
		if (end < header.length())
			domain = header.substr(token + 1, end - token - 1);
	}

	if (ban_domain_mng_) {
                if (auto dom_candidate = ban_domain_mng_->getDomainName(domain); dom_candidate) {
                        ++total_ban_domains_;
                        info->setIsBanned(true);
                        return;
                }
        }
        ++total_allow_domains_;

        attach_user_name(info, user_name);

	if (domain_mng_) {
                if (auto dom_candidate = domain_mng_->getDomainName(domain); dom_candidate) {
			++total_events_;
#if defined(BINDING)
                        if (dom_candidate->call.haveCallback())
                                dom_candidate->call.executeCallback(current_flow_);
#endif
                }
        }
}

void IMAPProtocol::processFlow(Flow *flow) {

	CPUCycle cycles(&total_cpu_cycles_);
	int length = flow->packet->getLength();
	total_bytes_ += length;
	++total_packets_;

	setHeader(flow->packet->getPayload());
	current_flow_ = flow;

        SharedPointer<IMAPInfo> info = flow->getIMAPInfo();
        if (!info) {
                if (info = info_cache_->acquire(); !info) {
			logFailCache(info_cache_->name(), flow);
			return;
                }
                flow->layer7info = info;
        }

        if (info->isBanned() == false) {
		boost::string_ref header(reinterpret_cast<const char*>(header_), length);

		if (flow->getFlowDirection() == FlowDirection::FORWARD) {
			// bypass the tag
			boost::string_ref client_cmd(header);
			size_t endtag = client_cmd.find(" ");

			client_cmd = client_cmd.substr(endtag + 1, length - (endtag));

			// Commands send by the client
			for (auto &command: commands_) {
				const char *c = std::get<0>(command);
				int offset = std::get<1>(command);

				if (std::memcmp(c, client_cmd.data(), offset) == 0) {
					int32_t *hits = &std::get<3>(command);
					int8_t cmd = std::get<4>(command);

					++(*hits);
					++total_imap_client_commands_;

					if (cmd == static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_LOGIN)) {
						int cmdoff = offset + endtag + 2;
						boost::string_ref header_cmd(header.substr(cmdoff, length - cmdoff));
						handle_cmd_login(info.get(), header_cmd);
					} else if (cmd == static_cast<int8_t>(IMAPCommandTypes::IMAP_CMD_STARTTLS)) {
						info->setStartTLS(true);
						// Force to write on the databaseAdaptor update method
						flow->packet->setForceAdaptorWrite(true);
					}
					info->incClientCommands();
					return;
				}
			}
		} else {
			++total_imap_server_responses_;
			info->incServerCommands();
			// Responses from the server

			// bypass the tag
			boost::string_ref server_cmd(header);
			size_t endtag = server_cmd.find(" ");

			server_cmd = server_cmd.substr(endtag + 1, length - (endtag));

			if (info->isStartTLS() and server_cmd[0] == 'O' and server_cmd[1] == 'K') {
				// Release the attached IMAPInfo object
				releaseFlowInfo(flow);
				// Reset the number of l7 packets, check SSLProtocol.cc
				flow->total_packets_l7 = 0;
				// Reset the forwarder so the next time will be a SSL flow
				flow->forwarder.reset();
			}
		}
	}
	return;
}

void IMAPProtocol::statistics(std::basic_ostream<char> &out, int level, int32_t limit) const {

	std::ios_base::fmtflags f(out.flags());

	showStatisticsHeader(out, level);

	if (level > 0) {
                if (ban_domain_mng_)
			out << "\t" << "Plugged banned domains from:" << ban_domain_mng_->name() << "\n";
                if (domain_mng_)
			out << "\t" << "Plugged domains from:" << domain_mng_->name() << "\n";
	}
	if (level > 3) {
		out << "\t" << "Total client commands:  " << std::setw(10) << total_imap_client_commands_ << "\n"
			<< "\t" << "Total server responses: " << std::setw(10) << total_imap_server_responses_ << "\n";

		for (auto &command: commands_) {
			const char *label = std::get<2>(command);
			int32_t hits = std::get<3>(command);
			out << "\t" << "Total " << label << ":" << std::right << std::setfill(' ') << std::setw(27 - strlen(label)) << hits << "\n";
		}
		out.flush();
	}
	if ((level > 5)and(flow_forwarder_.lock()))
		flow_forwarder_.lock()->statistics(out);
	if (level > 3) {
		info_cache_->statistics(out);
		user_cache_->statistics(out);
		if (level > 4)
			user_map_.show(out, "\t", limit);
	}
	out.flags(f);
}

void IMAPProtocol::statistics(Json &out, int level) const {

	showStatisticsHeader(out, level);

        if (level > 3) {
                Json j;

                out["allow"] = total_allow_domains_;
                out["banned"] = total_ban_domains_;
                out["requests"] = total_imap_client_commands_;
                out["responses"] = total_imap_server_responses_;

                for (auto &command: commands_)
                        j.emplace(std::get<2>(command), std::get<3>(command));

                out["commands"] = j;
        }
}

void IMAPProtocol::increaseAllocatedMemory(int value) {

        info_cache_->create(value);
        user_cache_->create(value);
}

void IMAPProtocol::decreaseAllocatedMemory(int value) {

        info_cache_->destroy(value);
        user_cache_->destroy(value);
}

CounterMap IMAPProtocol::getCounters() const {
	CounterMap cm;

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);
        cm.addKeyValue("commands", total_imap_client_commands_);
        cm.addKeyValue("responses", total_imap_server_responses_);

        for (auto &command: commands_) {
                const char *label = std::get<2>(command);

		cm.addKeyValue(label, std::get<3>(command));
        }

	return cm;
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict IMAPProtocol::getCacheData(const std::string &name) const {
#elif defined(RUBY_BINDING)
VALUE IMAPProtocol::getCacheData(const std::string &name) const {
#endif
        if (boost::iequals(name, "user"))
		return addMapToHash(user_map_);

	StringMap empty {"", ""};

        return addMapToHash(empty);
}

#if defined(PYTHON_BINDING)
SharedPointer<Cache<StringCache>> IMAPProtocol::getCache(const std::string &name) {

        if (boost::iequals(name, "user"))
                return user_cache_;

        return nullptr;
}

#endif

#endif

void IMAPProtocol::statistics(Json &out, const std::string &map_name, int32_t limit) const {

        if (boost::iequals(map_name, "users")) {
                user_map_.show(out, limit);
        }
}

void IMAPProtocol::resetCounters() {

	reset();

        total_events_ = 0;
        total_allow_domains_ = 0;
        total_ban_domains_ = 0;
        total_imap_client_commands_ = 0;
        total_imap_server_responses_ = 0;
        for (auto &command: commands_)
		std::get<3>(command) = 0;
}

} // namespace aiengine

