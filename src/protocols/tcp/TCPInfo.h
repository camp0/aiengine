/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_TCP_TCPINFO_H_ 
#define SRC_PROTOCOLS_TCP_TCPINFO_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "TCPStates.h"
#include "FlowInfo.h"
#include "json.hpp"

namespace aiengine {

class __attribute__ ((__packed__)) TCPInfo: public FlowInfo {
public:
    	explicit TCPInfo() { reset(); }
    	virtual ~TCPInfo() {}

        void reset();

	// TCP State
        int8_t state_prev;
        int8_t state_curr;

	// TCP Flags
	uint16_t syn;
	uint16_t syn_ack;
	uint16_t fin;
	uint16_t rst;
	uint16_t ack;
	uint16_t push;

        uint16_t window_size[2];

	bool handshake;
#if defined(HAVE_TCP_QOS_METRICS)
	// http://www.thevisiblenetwork.com/2015/04/12/5-key-tcp-metrics-for-performance-monitoring/
	uint16_t server_reset_rate;
	std::time_t last_sample_time;
	std::time_t connection_setup_time;
	std::time_t last_client_data_time;
	std::time_t application_response_time;
#endif
	// TCP Sequence numbers 0 for upstream and 1 for downstream FlowDirection
	uint32_t seq_num[2];

#if defined(BINDING)
	int getTotalSyns() const { return syn; }
	int getTotalSynAcks() const { return syn_ack; }
	int getTotalAcks() const { return ack; }
	int getTotalFins() const { return fin; }
	int getTotalRsts() const { return rst; }
	int getTotalPushs() const { return push; }
#endif
	const char *getState() const { return ((tcp_states[state_curr]).state)->name; }
        friend std::ostream& operator<< (std::ostream &out, const TCPInfo &info); 
	friend nlohmann::json& operator<< (nlohmann::json& out, const TCPInfo& info);
};

} // namespace aiengine
 
#endif  // SRC_PROTOCOLS_TCP_TCPINFO_H_
