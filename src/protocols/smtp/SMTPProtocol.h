/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_SMTP_SMTPPROTOCOL_H_ 
#define SRC_PROTOCOLS_SMTP_SMTPPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include "SMTPInfo.h"
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include <unordered_map>
#include "names/DomainNameManager.h"
#include "flow/FlowManager.h"
#include "FlowRegexEvaluator.h"

namespace aiengine {

enum class SMTPCommandTypes : std::int8_t {
	SMTP_CMD_EHLO = 	0,
	SMTP_CMD_AUTH ,  	
	SMTP_CMD_MAIL ,
	SMTP_CMD_RCPT ,
	SMTP_CMD_DATA ,
	SMTP_CMD_EXPN ,
	SMTP_CMD_VRFY ,
	SMTP_CMD_RSET ,
	SMTP_CMD_HELP ,
	SMTP_CMD_NOOP ,
	SMTP_CMD_STARTTLS ,
	SMTP_CMD_QUIT
};

// Commands with statistics
typedef std::tuple<const char*, int, const char*, int32_t, int8_t> SmtpCommandType;

class SMTPProtocol: public Protocol {
public:
	explicit SMTPProtocol():
		Protocol("SMTP", IPPROTO_TCP) {}
    	virtual ~SMTPProtocol();

	static const uint16_t header_size = 6; // Minimum header 220 \r\n;
	static const uint16_t MaxSMTPEmailLength = 512;

	uint16_t getId() const override { return 0x0000; }
	uint16_t getHeaderSize() const override { return header_size; }

	// Condition for say that a payload is SMTP 
	bool check(const Packet &packet) override;
	bool processPacket(Packet &packet) override { return true; }
	void processFlow(Flow *flow) override;

	void statistics(std::basic_ostream<char> &out, int level, int32_t limit) const override;
	void statistics(Json &out, int level) const override;
	void statistics(Json &out, const std::string &map_name, int32_t limit) const override;

        void setDomainNameManager(const SharedPointer<DomainNameManager> &dm) override; 
        void setDomainNameBanManager(const SharedPointer<DomainNameManager> &dm) override { ban_domain_mng_ = dm; }

	void releaseCache() override; 

        void setHeader(const uint8_t *raw_packet) override {
                
		header_ = raw_packet;
        }

	const uint8_t *getPayload() { return header_; }

        void increaseAllocatedMemory(int value) override; 
        void decreaseAllocatedMemory(int value) override; 

	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	uint64_t getCurrentUseMemory() const override;
	uint64_t getAllocatedMemory() const override;
	uint64_t getTotalAllocatedMemory() const override;

        void setDynamicAllocatedMemory(bool value) override;
        bool isDynamicAllocatedMemory() const override;

	uint32_t getTotalCacheMisses() const override;
	uint32_t getTotalEvents() const override { return total_events_; }

	CounterMap getCounters() const override; 
	void resetCounters() override;

#if defined(PYTHON_BINDING)
        boost::python::dict getCacheData(const std::string &name) const override;
        SharedPointer<Cache<StringCache>> getCache(const std::string &name) override;
#elif defined(RUBY_BINDING)
        VALUE getCacheData(const std::string &name) const;
#endif

	void setAnomalyManager(SharedPointer<AnomalyManager> amng) override { anomaly_ = amng; }

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	Flow *getCurrentFlow() { return current_flow_; }

        uint32_t getTotalClientCommands() const { return total_smtp_client_commands_; }
        uint32_t getTotalServerResponses() const { return total_smtp_server_responses_; }
#endif

	void releaseFlowInfo(Flow *flow) override;

private:
	uint64_t compute_memory_used_by_maps() const;
	void handle_cmd_mail(SMTPInfo *info, const boost::string_ref &header);
	void handle_cmd_rcpt(SMTPInfo *info, const boost::string_ref &header);
	void attach_from(SMTPInfo *info, const boost::string_ref &from);	
	void process_payloadl7(Flow * flow, SMTPInfo *info, const boost::string_ref &payloadl7);

	const uint8_t *header_ = nullptr;
        uint32_t total_events_ = 0;

	static std::vector<SmtpCommandType> commands_;
	
	uint32_t total_allow_domains_ = 0;	
	uint32_t total_ban_domains_ = 0;	
	uint32_t total_smtp_client_commands_ = 0;
	uint32_t total_smtp_server_responses_ = 0;

        SharedPointer<DomainNameManager> domain_mng_ = nullptr;
        SharedPointer<DomainNameManager> ban_domain_mng_ = nullptr;

        Cache<SMTPInfo>::CachePtr info_cache_ = Cache<SMTPInfo>::CachePtr(new Cache<SMTPInfo>("SMTP Info cache"));
        Cache<StringCache>::CachePtr from_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("From cache"));
        Cache<StringCache>::CachePtr to_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("To cache"));

        StringMap from_map_ {"SMTP Froms", "From"};
        StringMap to_map_ {"SMTP Tos", "To"};

	FlowManagerPtrWeak flow_mng_ = FlowManagerPtrWeak();
	Flow *current_flow_ = nullptr;
	SharedPointer<AnomalyManager> anomaly_ = nullptr;
	FlowRegexEvaluator eval_ {};
};

typedef std::shared_ptr<SMTPProtocol> SMTPProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_SMTP_SMTPPROTOCOL_H_
