/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2023  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_
#define SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <arpa/inet.h>
#include "Protocol.h"
#include "flow/FlowManager.h"
#include "Cache.h"

namespace aiengine {

struct ssh_header {
	uint32_t 	length;   	/* payload length */
    	uint8_t 	padding;        /* padding */
	uint8_t 	data[0];       
} __attribute__((packed));

// Some of the most common message types

class SSHProtocol: public Protocol {
public:
	explicit SSHProtocol():
		Protocol("SSH", IPPROTO_TCP) {}
    	virtual ~SSHProtocol() {}

	static constexpr uint16_t header_size = sizeof(ssh_header);

	uint16_t getId() const override { return 0x0000; }
	uint16_t getHeaderSize() const override { return header_size;}

	// Condition for say that a packet is ssh 
	bool check(const Packet &packet) override; 
        void processFlow(Flow *flow) override;
        bool processPacket(Packet& packet) override { return true; } 

	void statistics(std::basic_ostream<char>& out, int level, int32_t limit) const override;
	void statistics(Json &out, int level) const override;
	void statistics(Json &out, const std::string &map_name, int32_t limit) const override;

	void releaseCache() override; 

	void setHeader(const uint8_t *raw_packet) override { 

		header_ = raw_packet;
	}

	uint64_t getCurrentUseMemory() const override; 
	uint64_t getAllocatedMemory() const override;
	uint64_t getTotalAllocatedMemory() const override;

        void setDynamicAllocatedMemory(bool value) override;
        bool isDynamicAllocatedMemory() const override;

        uint32_t getTotalCacheMisses() const override;

        void increaseAllocatedMemory(int value) override;
        void decreaseAllocatedMemory(int value) override;

        void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	CounterMap getCounters() const override; 
	void resetCounters() override;

	Flow *getCurrentFlow() const { return current_flow_; }

	void releaseFlowInfo(Flow *flow) override;

#if defined(PYTHON_BINDING)
        boost::python::dict getCacheData(const std::string &name) const override;
	SharedPointer<Cache<StringCache>> getCache(const std::string &name) override;
#elif defined(RUBY_BINDING)
        VALUE getCacheData(const std::string &name) const;
#endif

#if defined(STAND_ALONE_TEST) || defined(TESTING)
        uint32_t getTotalHandshakePDUs() const { return total_handshake_pdus_; }
	uint32_t getTotalAlgorithmNegotiationMessages() const { return total_algorithm_negotiation_messages_; }
	uint32_t getTotalKeyExchangeMessages() const { return total_key_exchange_messages_; }
	uint32_t getTotalOthers() const { return total_others_; }

	uint64_t getTotalEncryptedBytes() const { return total_encrypted_bytes_; }
	uint32_t getTotalEncryptedPackets() const { return total_encrypted_packets_; }
#endif

private:
	bool is_minimal_ssh_header(const uint8_t *hdr);
	void attach_ssh_client_name(SSHInfo *info, const boost::string_ref &name);
	void attach_ssh_server_name(SSHInfo *info, const boost::string_ref &name);
        uint64_t compute_memory_used_by_maps() const;

	const uint8_t *header_ = nullptr;

	// Some statistics 
	uint64_t total_encrypted_bytes_ = 0;
	uint32_t total_encrypted_packets_ = 0;
	uint32_t total_handshake_pdus_ = 0;
	uint32_t total_algorithm_negotiation_messages_ = 0; // messages from 20 to 29
	uint32_t total_key_exchange_messages_ = 0; // messages from 30 to 49
	uint32_t total_others_ = 0;

	Cache<SSHInfo>::CachePtr info_cache_ = Cache<SSHInfo>::CachePtr(new Cache<SSHInfo>("SSH Info cache"));
        Cache<StringCache>::CachePtr name_cache_ = Cache<StringCache>::CachePtr(new Cache<StringCache>("Name cache"));

        StringMap name_map_ {"Names", "Name"};

	FlowManagerPtrWeak flow_mng_ = FlowManagerPtrWeak();
        Flow *current_flow_ = nullptr;
};

typedef std::shared_ptr<SSHProtocol> SSHProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_
